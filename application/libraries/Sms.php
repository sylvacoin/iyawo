<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Sms 
{
	protected $CI;
    protected $db;
    protected $config;
    protected $sms_config;
    protected $sms;
    protected $table;

   	public function __construct($param = null)
   	{
   		$this->CI = & get_instance();
        $this->db = $this->CI->db;
        $this->table = 'settings';
   	}

    public function send_sms($data)
	{
		//$data = [to, message]
		extract($data);
		$token 		= $this->get_setting('sms_api_token');
		$baseurl 	= $this->get_setting('sms_api');
		$senderid 	= $this->get_setting('sms_sender_id');
		$status 	= $this->get_setting('sms_response_key');
		$success = $this->get_setting('sms_response_success');

		$baseurl = str_replace('%(TOKEN)', $token, $baseurl);
		$baseurl = str_replace('%(MESSAGE)', urlencode($message), $baseurl);
		$baseurl = str_replace('%(SENDER)', urlencode($senderid), $baseurl);
		$baseurl = str_replace('%(TO)', urlencode($to), $baseurl);

		//$this->debugger->debug($success);

		$ch = curl_init(); 

		curl_setopt($ch, CURLOPT_URL,$baseurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

		$response = curl_exec($ch);

		curl_close($ch);
		$res = json_decode($response);

		if ( $res->$status == $success)
		{
			return true;
		} // response code

		return false;
	}

	public function get_setting( $key )
	{
		
		$this->CI->db->where('skey', $key);
		$result = $this->CI->db->get($this->table);

		if ( $result->num_rows() > 0 )
		{
			return $result->row()->svalue;
		}
		return null;
	}

}