<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MailerLite {

    protected $CI;
    protected $db;
    protected $config;
    protected $mail_config;
    protected $mail;

    public function __construct( array $input) {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->db = $this->CI->db;
        $this->CI->load->library('email');

        $this->config = (object) array(
            'fields' => isset($input['fields'])?$input['fields']:'',
            'template' => isset($input['template'])?$input['template']:'',
            'to' => $input['to']
        );


        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'mail.itechplanetpc.com';
        $config['smtp_port'] = 26;
        $config["smtp_user"] = 'developer@itechplanetpc.com';
        $config["smtp_pass"] = 'itechdeveloper92';
        $config['priority'] = 1;

        $config['useragent'] = SITENAME;
        $config['mailtype'] = 'html';


        $this->CI->email->initialize($config);

        $this->CI->email->set_newline("\r\n");
        $this->CI->email->from('noreply@iyawosavings.com', SITENAME);

        $this->CI->email->to($this->config->to);
    }

    public function get_mail_option() {
        $this->db->where(['action' => $this->config->template]);
        $mail = $this->db->get('mail_templates');
       
        if ($mail->num_rows() == 0 )
        {
            die('Invalid mail action on get mail option');
        }

        return $mail->row();
    }

    public function add_attachments( array $attachments)
    {
        foreach ( $attachments as $name => $path)
        {
            $this->CI->email->attach($path, 'attachment', $name);
        }

    }

    public function add_ccs( $ccs)
    {
        $this->CI->email->cc($ccs);

    }

    public function add_bccs( $bccs)
    {
        $this->CI->email->bcc($bccs);

    }


    public function add_address( $addresses )
    {
        $this->CI->email->to($addresses);
    }

    public function send_mail() {
        // add any data you want in this array
        $fields = $this->config->fields;

        try {
            $mail_template = $this->get_mail_option();
            $template = $mail_template->body;
            $message = str_replace(array_keys($fields), array_values($fields), $template);

            if ( ! $mail_template )
            {
                return false;
            }
            $this->CI->email->subject($mail_template->subject);
            $this->CI->email->message($message);
            // send email
            $this->CI->email->send();
            return true;
            
        } catch (phpmailerException $e) {
            log_message('error', $e->errorMessage()); //Pretty error messages from PHPMailer
            return false;
        } catch (Exception $e) {
            log_message('error', $e->getMessage()); //Boring error messages from anything else!
            return false;
        }

    }
    
    function send($subject, $message)
    {
        try {
           	$this->CI->email->Subject = $subject;
            $this->CI->email->Body    = $message;
            // send email
            $this->CI->email->send(FALSE);
            
            return true;
        } catch (phpmailerException $e) {
            log_message('error', $e->errorMessage()); //Pretty error messages from PHPMailer
            return false;
        } catch (Exception $e) {
            log_message('error', $e->getMessage()); //Boring error messages from anything else!
            return false;
        }
    }

}
