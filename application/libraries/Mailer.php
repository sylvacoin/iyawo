<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer {

    protected $CI;
    protected $db;
    protected $config;
    protected $mail_config;
    protected $mail;

    public function __construct( array $config) {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->db = $this->CI->db;
        $this->mail = new PHPMailer(true);

        $this->config = (object) array(
            'fields' => isset($config['fields'])?$config['fields']:'',
            'template' => isset($config['template'])?$config['template']:'',
            'to' => $config['to']
        );

        //Server settings
        $this->mail->SMTPDebug = 3; 
        $this->mail->DebugOutput = "error_log";                                       // Enable verbose debug output
        $this->mail->isSMTP();                                            // Set mailer to use SMTP
        $this->mail->Host       = 'mail.aianet.com.ng';  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $this->mail->Username   = 'no-reply@aianet.com.ng';                     // SMTP username 
        $this->mail->Password   = 'xM-y&4K3{{Z+';                               // SMTP password
        $this->mail->Port       = 26;
        $this->mail->setFrom('noreply@iyawosavings.com', SITENAME);
        $this->mail->isHTML(true);
        $this->mail->addAddress($this->config->to);
    }

    public function get_mail_option() {
        $this->db->where(['action' => $this->config->template]);
        $mail = $this->db->get('mail_templates');

        if ($mail->num_rows() == 0 )
        {
            die('Invalid mail action on get mail option');
        }

        return $mail->row();
    }

    public function add_attachments( array $attachments)
    {
        foreach ( $attachments as $name => $path)
        {
            $this->mail->addAttachment($path, $name);
        }

    }

    public function add_ccs( array $ccs)
    {
        foreach ( $ccs as $email)
        {
            $this->mail->addCC($email);
        }

    }

    public function add_bccs( array $bccs)
    {
        foreach ( $bccs as $email)
        {
            $this->mail->addBCC($email);
        }

    }


    public function add_address( array $addresses )
    {
        foreach ( $addresses as $address )
        {
            $this->mail->addAddress($address);               // Name is optional
        }
    }

    public function send_mail() {
        // add any data you want in this array
        $fields = $this->config->fields;
        try {
            $mail_template = $this->get_mail_option();
            $template = $mail_template->body;
            $message = str_replace(array_keys($fields), array_values($fields), $template);
            

            if ( ! $mail_template )
            {
                return false;
            }
            $this->mail->Subject = $mail_template->subject;
            $this->mail->Body    = $message;
            // send email
            $this->mail->send();
            
            return true;
        } catch (phpmailerException $e) {
            log_message('error', $e->errorMessage()); //Pretty error messages from PHPMailer
            return false;
        } catch (Exception $e) {
            log_message('error', $e->getMessage()); //Boring error messages from anything else!
            return false;
        }

    }
    
    function send($subject, $message)
    {
        try {
            $this->mail->Subject = $subject;
            $this->mail->Body    = $message;
            // send email
            $this->mail->send();
            
            return true;
        } catch (phpmailerException $e) {
            log_message('error', $e->errorMessage()); //Pretty error messages from PHPMailer
            return false;
        } catch (Exception $e) {
            log_message('error', $e->getMessage()); //Boring error messages from anything else!
            return false;
        }
    }

}
