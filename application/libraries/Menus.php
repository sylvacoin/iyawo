<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus {

	protected $CI;
	protected $db;
	private $table;
	private $result;
	public $config = array();
	public $debugger;

	public function __construct() {
		$this->CI = &get_instance();
		$this->db = $this->CI->db;
		$this->table = "menus";
		$this->debugger = $this->load('debugger', 'library');
	}

	public function get_menu($location = '', $role = '') {
		$menus = array();

		//get the usergroup menus
		$this->db->where('user_group_id', $role);
		$query = $this->db->get('user_groups');

		if ($query->num_rows() > 0) {
			$menu_list = $query->row()->user_group_menus;
			$this->db->where('parent_id', 0);
			$this->db->order_by('menu_order');
			if ($location) {
				$this->db->where('menu_location', $location);
			}
			$this->db->where_in('menu_id', explode(',', $menu_list));
			
			$result = $this->db->get($this->table)->result_array();
			//$this->debugger->debug($result);
			foreach ($result as $key => $menu) {
				$menus[$key] = $menu;
				$this->db->order_by('menu_order');
				$this->db->where_in('menu_id', explode(',', $menu_list));
				$sub_menus = $this->db->get_where($this->table, ['parent_id' => $menu['menu_id']])->result();
				$menus[$key]['sub_menus'] = $sub_menus;
			}

		}

		return $menus;
	}

	public function get_menu_ml($location = '', $role = '', $access_level_all = true)
	{
		// code...
		$menus = array();

		//get the usergroup menus
		$this->db->where('user_group_id', $role);
		$query = $this->db->get('user_groups');
		if ($query->num_rows() > 0) {
			$menu_list = $query->row()->user_group_menus;
			$menus = $this->get_all_menus(0,$menu_list, $location, $access_level_all);
		}
		//$this->debugger->debug($menus);
		return $menus;
	}

	public function get_all_menus($parent_id, $menu_list, $location, $all = false)
	{
		$menus = array();
		$this->db->where('parent_id', $parent_id);
		if ($location) {
			$this->db->where('menu_location', $location);
		}
		$this->db->order_by('menu_order');
		if ( !$all )
		{
			$this->db->where_in('menu_id', explode(',', $menu_list));
		}
		$result = $this->db->get($this->table)->result_array();
		//$this->debugger->debug($result);
		if (count($result) > 0 ){
			foreach ($result as $key => $menu) {
				$menus[$key] = $menu;
				$menus[$key]['sub_menus'] = $this->get_all_menus($menu['menu_id'], $menu_list, $location, $all);
			}
		}
		return $menus;
	}


	public function all($location = "frontend") {
		$html = "";

		$menus = $this->get_menu();

		//echo array_count_values( $menus['location']);
		if ($location == "frontend") {
			$menu_list = array_filter($menus, function ($k) {
				return $k->location == "frontend";

			}, ARRAY_FILTER_USE_BOTH);

			if (count($menu_list) > 2) {
				$midPoint = round(count($menu_list) / 2);
			} else {
				$midPoint = 1;
			}
			$count = 0;

			$html .= "\n" . '<' . $this->config['container'] . ' class="' . $this->config['container_class'] . '" id="' . $this->config['container_id'] . '">' . "\n";
			$html .= "\t\t\t<" . $this->config['sub_container'] . ' class="' . $this->config['sub_container_class'] . '" id="' . $this->config['sub_container_id'] . '">' . "\n";
			foreach ($menu_list as $menu) {
				$count++;

				$html .= '<' . $this->config['link_tag'] . ' class="' . $this->config['link_tag_class'] . '" href="' . base_url('action/') . $menu->url . '">' . $menu->menu . '</' . $this->config['link_tag'] . '>' . "\n";
				if ($midPoint == $count) {
					$html .= '<a class="navbar-brand mx-2 d-none d-md-inline" href="' . base_url() . '"><img src="' . base_url() . '/assets/img/logo.png" alt="logo" /></a>' . "\n";
				}
			}

			$html .= "\t\t\t </" . $this->config['sub_container'] . ">\n";
			$html .= "</" . $this->config['container'] . "> \n\n";

			return $html;
		}
	}

	private function load($item, $item_type = 'model') {
		$this->CI->load->$item_type($item);
		return $this->CI->$item;
	}

}

/* End of file Menus.php */
/* Location: ./application/libraries/Menus.php */
