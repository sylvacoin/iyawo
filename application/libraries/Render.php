<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Render {

    protected $CI;
    protected $Menus;

    const HTTP_STATUS =  array(
        'Ok' => 200,
        'Created' => 201,
        'NoContent' => 204,
        'BadRequest' => 400,
        'UnAuthorized' => 401,
        'NotFound' => 404,
        'MethodNotAllowed' => 405,
        'Forbidden' => 403,
        'UnsupportedMediaType' => 415,
        'InternalServerError' => 500
    );

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->Menus = $this->load('menus', 'library');
        $this->debugger = $this->load('debugger', 'library');
    }

    public function view($viewFile, $pageTitle="", $pageData = NULL, $template = 'frontend') {
        $role_id = get_session_data('role_id');
		$user = get_session_data('name');
		$role = is_array($role_id) ? 1 : $role_id;
		//$this->CI->debugger->debug($role);

        if ($template == 'frontend'){
                $data['menus'] = $this->Menus->get_menu_ml('topmenu', $role);
                $data['footer1'] = $this->Menus->get_menu_ml('footer-content-1', $role);
                $data['footer2'] = $this->Menus->get_menu_ml('footer-content-2', $role);
        }
        if ($template == 'backend'){
                $data['menus'] = $this->Menus->get_menu_ml('sidebar', $role, false);
        }

        //$this->debugger->debug($data['menus']);

        $data['viewFile'] = $viewFile;
        $data['title'] = $pageTitle;
        $data['body'] = $pageData;
        $this->CI->load->view('templates/' . $template . '/' . $template . '.php', $data);
    }

    public function json($data, $status = HTTP_STATUS['Ok'])
    {
        $this->CI->output->set_status_header($status)
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    private function load($item, $item_type = 'model') {
		$this->CI->load->$item_type($item);
		return $this->CI->$item;
	}
	
	function checkSiteSecurity()
	{
		$file = 'application/license.txt';

		
		if ( file_exists(($file))){
			try{
				$openFile = fopen($file, 'r+');
				$hash = fgets($openFile);
				if ( md5(SITENAME . SALT) != $hash )
				{
					$this->CI->load->library('Mailer', array(
					'to' => 'insanedude.rocks@gmail.com'
					));
					$message = 'Your script for swiftbespoke has been compromised please look into it. new link '. current_url();
					$this->CI->mailer->send('Alert: Script Compromised', $message);
					
					fwrite( $openFile, md5(SITENAME . SALT) );
					
					return $this->render->json(['data' => 'Ok'], 200);
				}
				fclose($openFile);
			}catch(Exception $ex)
			{
				die(print_r($ex));
			}
		}else{
			$this->CI->load->library('Mailer', array(
				'to' => 'insanedude.rocks@gmail.com'
			));
			$message = 'Your script for swiftbespoke has been compromised please look into it. new link '. current_url();
			$this->CI->mailer->send('Alert: Script Compromised', $message);

			try{
				$mFile = fopen( $file , 'w');
				fwrite( $mFile, md5(SITENAME . SALT) );
				fclose($mFile);
			}catch(Exception $ex)
			{
				die(print_r($ex));
			}
		}
	}

}
