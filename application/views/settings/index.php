<div id="app">

  <!-- ROW ENDS -->
  <div class="row section social-section">
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark " style="flex-direction: column !important;">
        <div class="content">
          <h4 class="d-block">Reset all customer cards</h4>
          <button class="btn btn-primary btn-md  d-block" @click="doGlobalCardReset()">Reset</button>
        </div>
      </div>
    </div>
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark " style="flex-direction: column !important;">
        <div class="content">
          <h4 class="d-block">Reset all customer data</h4>
          <button class="btn btn-primary btn-md  d-block" @click="doGlobalCustomerReset()">Reset</button>
        </div>
      </div>
    </div>
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark " style="flex-direction: column !important;">
        <div class="content">
          <h4 class="d-block">Reset system data</h4>
          <button class="btn btn-primary btn-md  d-block" @click="doGlobalReset()">Reset</button>
        </div>
      </div>
    </div>

  </div>
  <!-- ROW ENDS -->

</div>

<script type="text/javascript">
  var app  = new Vue({
    el:'#app',
    data: {
      base_url:base_url,
    },
    methods: {
      //get transactions for this user,
      doGlobalCardReset(card){
        var confirm = window.confirm("Do you want to reset all card transactions? Note data cannot be retrieved after it has been erased");

        if (confirm)
        {
      

        axios.post(base_url+'transactions/reset-all-card-transactions').then((response) => {
          
          if ( response.data.status == 200)
          {
            $.growl.notice({message:response.data.message});
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400)
          {
            $.growl.error({ message: response.data.message});
          }
          this.isLoading = false;
        }).catch((e)=>{
          
          $.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });
        }

      },
      doGlobalCustomerReset(card){
        var confirm = window.confirm("Do you want to reset all customer data? Note data cannot be retrieved after it has been erased");

        if (confirm)
        {
      

        axios.post(base_url+'customers/reset-customers').then((response) => {
          
          if ( response.data.status == 200)
          {
            $.growl.notice({message:response.data.message});
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400)
          {
            $.growl.error({ message: response.data.message});
          }
          this.isLoading = false;
        }).catch((e)=>{
          
          $.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });
        }

      },

      doGlobalReset(card){
        var confirm = window.confirm("Do you want to reset system? Note data cannot be retrieved after it has been erased");

        if (confirm)
        {
      

        axios.post(base_url+'reset').then((response) => {
          
          if ( response.data.status == 200)
          {
            $.growl.notice({message:response.data.message});
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400)
          {
            $.growl.error({ message: response.data.message});
          }
          this.isLoading = false;
        }).catch((e)=>{
          
          $.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });
        }

      },

    },
  })
</script>