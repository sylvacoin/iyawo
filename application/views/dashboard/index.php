  <div id="app">
  
<?php $role = get_session_data('role_id'); if ( is_admin($role) ) : ?>

    <!-- ROW ENDS -->
  <div class="row section social-section">
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="social-card w-100 bg-success">
        <div class="social-icon">
          <i class="icon-wallet icons"></i>
        </div>
        <div class="content">
          <p><?= DEFAULT_CURRENCY ?>{{ formatPrice(totalBalance) }}</p> 
          <p>Overall Client Balance</p>
        </div>
      </div>
    </div>
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="social-card w-100 bg-success">
        <div class="social-icon">
          <i class="icon-wallet icons"></i>
        </div>
        <div class="content">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(totalProfit)}}</p>
          <p>Overall Profit</p>
        </div>
      </div>
    </div>
  </div>

  <div class="row section social-section">
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100 bg-twitter justify-left">
        <div class="social-icon">
          <i class="icon-pie-chart icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(dailyProfit)}}</p>
          <p>Daily Profit</p>
        </div>
      </div>
    </div>
    
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100  bg-twitter justify-left">
        <div class="social-icon">
          <i class="icon-pie-chart icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(monthlyProfit)}}</p>
          <p>Overall Monthly Profit</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100 bg-dribbble justify-left">
        <div class="social-icon">
          <i class="icon-pie-chart icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(totalPendingWithdrawal)}}</p>
          <p>Pending withdrawals (PW</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark">
        <div class="social-icon">
          <i class="icon-wallet icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(totalDaily)}}</p>
          <p>Daily Total</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark">
        <div class="social-icon">
          <i class="icon-people icons"></i>
        </div>
        <div class="content content-small">
          <p>{{totalCustomers}}</p>
          <p>Clients</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark">
        <div class="social-icon">
          <i class="icon-user icons"></i>
        </div>
        <div class="content content-small">
          <p>{{totalHandlers}}</p>
          <p>Handlers</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark">
        <div class="social-icon">
          <i class="icon-bell icons"></i>
        </div>
        <div class="content content-small">
          <p>{{PendingWithdrawal}}</p>
          <p>PW count</p>
        </div>
      </div>
    </div>
  </div>

<div class="row">
    <div class="col-lg-12 grid-margin">

      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"></h2>
          <div class="tab-minimal">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="tab-2-1" data-toggle="tab" href="#home-2-1" role="tab" aria-controls="home-2-1" aria-selected="true"><i class="mdi mdi-home-outline"></i>Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tab-2-2" data-toggle="tab" href="#profile-2-2" role="tab" aria-controls="profile-2-2" aria-selected="false"><i class="mdi mdi-chart-pie"></i>Report</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade show active" id="home-2-1" role="tabpanel" aria-labelledby="tab-2-1">
                <div class="row">
                  <div class="col-6 border-right">
                    <h1>Last 10 Deposit</h1>
                    <div class="table-responsive">  
                      <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Handler</th>
                          <th>Customer</th>
                          <th>Amount</th>
                          <th>Card no</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr v-for="row in creditTrans">
                          <td>{{row.firstname}} {{row.lastname}}</td>
                          <td>{{row.full_name}}</td>
                          <td class="text-success"> <?= DEFAULT_CURRENCY ?> {{formatPrice(row.amount)}}
                          </td>
                          <td><label class="badge badge-success">{{row.card_no}}</label></td>
                        </tr>
                      </tbody>
                      </table>
                    </div>
                
                  </div>
                  <div class="col-6 pl-4">
                   <h1>Last 10 Pending Withdrawal Request</h1>
                    <div class="table-responsive">
                      <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Handler</th>
                          <th>Customer</th>
                          <th>Amount</th>
                          <th>Card no</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        <tr v-for="row in debitTrans">
                          <td>{{row.firstname}} {{row.lastname}}</td>
                          <td>{{row.full_name}}</td>
                          <td class="text-danger"> <?= DEFAULT_CURRENCY ?> {{formatPrice(row.amount)}}
                          </td>
                          <td><label class="badge badge-danger">{{row.card_no}}</label></td>                  
                        </tr>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="profile-2-2" role="tabpanel" aria-labelledby="tab-2-2">
                <div class="align-items-center mb-5 justify-content-between d-lg-flex d-xl-flex d-md-block d-block">
                  <h2>{{canvasTitle}}</h2>
                  <div id="chartLegend"></div>
                  <div class="nav-wrapper d-inline-block mt-4 mt-lg-0">
                    <ul class="nav nav-pills">
                      <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)" active  @click="getWeek()">Week</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)" @click="getMonth()">Month</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)"  @click="getYear()">Year</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="float-chart-mini"  style="height:300px; width:100%">
                  <canvas id="myChart" width="300" height="200"></canvas>
                </div>
              </div>
             
            </div>
          </div>

          
          
        </div>
       
      </div>
    </div>
</div>
<?php else: ?>
 <div class="row section social-section">
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="social-card w-100 bg-success">
        <div class="social-icon">
          <i class="icon-wallet icons"></i>
        </div>
        <div class="content">
          <p><?= DEFAULT_CURRENCY ?>{{ formatPrice(totalBalance) }}</p> 
          <p>Overall Balance</p>
        </div>
      </div>
    </div>
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="social-card w-100 bg-success">
        <div class="social-icon">
          <i class="icon-wallet icons"></i>
        </div>
        <div class="content">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(totalProfit)}}</p>
          <p>Overall Profit</p>
        </div>
      </div>
    </div>
  </div>

  <div class="row section social-section">
    <div class="col-lg-3 grid-margin stretch-card">
      <div class="social-card w-100 bg-twitter justify-left">
        <div class="social-icon">
          <i class="icon-pie-chart icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(dailyProfit)}}</p>
          <p>Daily Profit</p>
        </div>
      </div>
    </div>
    
    <div class="col-lg-4 grid-margin stretch-card">
      <div class="social-card w-100  bg-twitter justify-left">
        <div class="social-icon">
          <i class="icon-pie-chart icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(monthlyProfit)}}</p>
          <p>Overall Monthly Profit</p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark">
        <div class="social-icon">
          <i class="icon-wallet icons"></i>
        </div>
        <div class="content content-small">
          <p><?= DEFAULT_CURRENCY ?>{{formatPrice(totalDaily)}}</p>
          <p>Daily Total</p>
        </div>
      </div>
    </div>
    <div class="col-lg-2 grid-margin stretch-card">
      <div class="social-card w-100 bg-white text-dark">
        <div class="social-icon">
          <i class="icon-people icons"></i>
        </div>
        <div class="content content-small">
          <p>{{totalCustomers}}</p>
          <p>Clients</p>
        </div>
      </div>
    </div>
  </div>

<div class="row">
  
            <div class="col-lg-12 grid-margin">
              <div class="card overflow-hidden dashboard-curved-chart">
                <div class="card-body mx-3">
                  <h2 class="card-title border-bottom-none">{{canvasTitle}}</h2>
                  <div class="align-items-center mb-5 justify-content-between d-lg-flex d-xl-flex d-md-block d-block">
                    <div id="chartLegend"></div>
                    <div class="nav-wrapper d-inline-block mt-4 mt-lg-0">
                      <ul class="nav nav-pills">
                        <li class="nav-item">
                          <a class="nav-link" href="javascript:void(0)" active  @click="getWeek()">Week</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="javascript:void(0)" @click="getMonth()">Month</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="javascript:void(0)"  @click="getYear()">Year</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="float-chart-mini"  style="height:300px; width:100%">
                    <canvas id="myChart" width="300" height="200"></canvas>
                  </div>
                  
                </div>
               
              </div>
            </div>
  </div>
<?php endif; ?>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script type="text/javascript">

  var app = new Vue({
    el: '#app',
    data: {
      PendingWithdrawal:0,
      totalCustomers: 0,
      totalDaily: 0,
      totalHandlers: 0,
      totalWeekly:0,
      totalMonthly:0,
      totalBalance:0,
      totalProfit:0,
      dailyProfit:0,
      monthlyProfit:0,
      totalPendingWithdrawal: 0,
      ajaxresponse:{},
      canvasTitle:"",
      debitTrans:[],
      creditTrans:[]
    },
    beforeMount(){
      this.init();
    },
    methods:{
      init(){
        axios.get(base_url+'transactions/getAjaxTransactionStatistics').then((response) => {
          if (response.status == 200 )
          {
            this.ajaxresponse = response.data.data;
            this.totalDaily = response.data.data.todaysTotal == null ? 0 : response.data.data.todaysTotal;
            this.totalWeekly = response.data.data.weeklyTotal == null ? 0 : response.data.data.weeklyTotal;
            this.totalMonthly = response.data.data.monthlyTotal == null ? 0 : response.data.data.monthlyTotal;
            this.totalBalance = response.data.data.totalBalance == null ? 0 : response.data.data.totalBalance;
            this.totalPendingWithdrawal = response.data.data.totalWithdrawalBalance == null ? 0 : response.data.data.totalWithdrawalBalance;
            this.PendingWithdrawal = response.data.data.PendingWithdrawalCount == null ? 0 : response.data.data.PendingWithdrawalCount;
            this.totalProfit = response.data.data.totalProfit == null ? 0 : response.data.data.totalProfit;
            this.dailyProfit = response.data.data.dailyProfit == null ? 0 : response.data.data.dailyProfit;
            this.monthlyProfit = response.data.data.monthlyProfit == null ? 0 : response.data.data.monthlyProfit;

            let rows = response.data.data.weeklyTransactions;
            let mydata = [];
            let mylabels = [];
            for(row in rows)
            {
              mylabels.push( rows[row].date );
              mydata.push( parseInt(rows[row].total) );
            }
            this.canvasTitle = "weekly Contributions";
            this.renderChart(mydata, mylabels,'Total Daily Contributions made');
          }
        }).then(()=>{
          axios.get(base_url+'api/count-customers').then((response) => {
            if (response.status == 200 )
            {
              this.totalCustomers = response.data.data;
            }
          })
         axios.get(base_url+'api/count-users').then((response) => {
            if (response.status == 200 )
            {
              this.totalHandlers = response.data.data;
            }
          })
          axios.get(base_url + 'transactions/get-transactions/debit').then((resp) => {
            if ( resp.status == 200 )
            {
              this.debitTrans = resp.data.data;
            }
          })
          axios.get(base_url + 'transactions/get-transactions/credit').then((resp) => {
            if ( resp.status == 200 )
            {
              this.creditTrans = resp.data.data;
            }
          })
        });
      },
      getMonth()
      {
        this.canvasTitle = "Monthly Contributions";
        let rows = this.ajaxresponse.monthlyTransactions;
        let mydata = [];
        let mylabels = [];
        for(row in rows)
        {
          mylabels.push( rows[row].date );
          mydata.push( parseInt(rows[row].total) );
        }
        this.renderChart(mydata, mylabels,'Total Monthly Contributions made');
      },
      getYear()
      {
        this.canvasTitle = "Yearly Contributions";
        let rows = this.ajaxresponse.yearlyTransactions;
        let mydata = [];
        let mylabels = [];
        for(row in rows)
        {
          mylabels.push( rows[row].date );
          mydata.push( parseInt(rows[row].total) );
        }
        this.renderChart(mydata, mylabels,'Total yearly Contributions made');
      },
      getWeek()
      {
        this.canvasTitle = "Weekly Contributions";
        let rows = this.ajaxresponse.weeklyTransactions;
        let mydata = [];
        let mylabels = [];
        for(row in rows)
        {
          mylabels.push( rows[row].date );
          mydata.push( parseInt(rows[row].total) );
        }
        this.renderChart(mydata, mylabels,'Total weekly Contributions made');
      },
      formatPrice(value) {
          let val = (value/1).toFixed(2);
          return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
      },
      renderChart(data, labels, label, type="bar")
      {
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
          type: type,
          data: {
            labels: labels,
            datasets: [
              {
                label: label,
                data: data,
                backgroundColor: '#f87979',
                borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  barThickness: 4,
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              },
              responsive: true, maintainAspectRatio: false
          }
        });
      }
    },
  });
</script>
