<?php
$this->load->view('templates/frontend/header');     
if (!isset($viewFile)) {
    $viewFile = "";
}

if (!isset($body)) {
    $body = NULL;
}

if ($viewFile != "" ) {
    $this->load->view($viewFile, $body);
} else {
    echo nl2br("Houston we have a problem. You omitted a viewFile");
}

$this->load->view('templates/frontend/footer');
