<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= isset($title)?$title:'' ?> | <?= SITENAME ?></title>
    <!-- plugins:css -->
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?= base_url()  ?>assets/css/jquery.growl.css">
  <script type="text/javascript"> var base_url = '<?= base_url(); ?>'</script>

    <link rel="stylesheet" href="<?= base_url()  ?>assets/css/vuetify.min.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?= base_url()  ?>assets/images/favicon.png" />
  <script src="<?= base_url() ?>assets/vendor/jquery/dist/jquery.min.js"></script> 
  <script src="<?= base_url() ?>assets/js/custom/vue.min.js"></script>  
  <script src="<?= base_url() ?>assets/js/custom/jquery.growl.js"></script> 
  <script src="<?= base_url() ?>assets/js/custom/axios.min.js"></script> 
  
</head>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
