<nav class="navbar navbar-dark navbar-expand-md navbar-toggleable-sm" data-toggle="affix">
    <div class="container">
        <a class="navbar-brand" href="<?= base_url() ?>#">
            <img src="<?= base_url() ?>assets/img/ASave LOGO White.png" id="origLogo">
            <img src="<?= base_url() ?>assets/img/ASave LOGO Red.png" id="altLogo" class=" pulse animated infinite">
        </a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div
            class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link <?= set_active('home') ?>" href="<?= base_url() ?>">Home</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link <?= set_active('gallery') ?>" href="<?= base_url() ?>gallery.html">Gallery</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link <?= set_active('donate') ?>" href="<?= base_url() ?>donate.html">Donate</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link <?= set_active('about') ?>" href="<?= base_url() ?>#about">About</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link <?= set_active('contact') ?>" href="<?= base_url() ?>contact.html">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>