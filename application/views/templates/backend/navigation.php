 <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper">
        <a class="navbar-brand brand-logo" href="<?= site_url() ?>"><img src="<?=base_url()?>assets/images/logo.gif" alt="logo"></a>
        <a class="navbar-brand brand-logo-mini" href="<?= site_url() ?>"><img src="<?=base_url()?>assets/images/logo_mini.svg" alt="logo"></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler d-none d-lg-block align-self-center mr-2" type="button" data-toggle="minimize">
        <span class="icon-list icons"></span>
      </button>
      <?php $username = get_session_data('name'); ?>
        <p class="page-name d-none d-lg-block">Hi, <?=!empty($username) ? $username : '';?></p>
        <ul class="navbar-nav ml-lg-auto">
          <li class="nav-item">
            <p class="page-name d-none d-lg-block" style="margin-right: 50px;"> <small><?= get_system_overall_balance(); ?> </small></p>
          </li>
          
         
          <li class="nav-item d-none d-sm-block profile-img">
            <a class="nav-link profile-image" href="#">
                <?php if( get_session_data('email') !== '' ): ?>
                <?php $grav_url = get_gravatar(get_session_data('email') ); ?>
                <?php endif; ?>
              <img src="<?= $grav_url ?>" alt="profile-img">
              <span class="online-status online bg-success"></span>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center ml-auto" type="button" data-toggle="offcanvas">
          <span class="icon-menu icons"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
