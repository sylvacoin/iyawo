<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= isset($title)?$title:'' ?> | <?= SITENAME ?></title>
    <!-- plugins:css -->
    <link rel="shortcut icon" href="<?= base_url()  ?>assets/images/favicon.png" />
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="<?= base_url()  ?>assets/vendor/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?= base_url()  ?>assets/css/jquery.growl.css">
  <!-- inject:css -->

  
  <link rel="stylesheet" href="<?= base_url()  ?>assets/css/style.css">
  <!-- endinject -->
  <script type="text/javascript"> var base_url = '<?= base_url(); ?>'</script>
  
  <script src="<?= base_url() ?>assets/vendor/jquery/dist/jquery.min.js"></script> 
   <script src="<?= base_url() ?>assets/js/globaljs.js"></script> 
  <?php if (ENVIRONMENT == 'production'): ?>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
  <?php else: ?>
  <script src="<?= base_url() ?>assets/js/custom/vue.min.js"></script>
  <?php endif;?>

  
  <script src="<?= base_url() ?>assets/node_modules/vue-bootstrap4-table/dist/vue-bootstrap4-table.min.js"></script>  
  <script src="<?= base_url() ?>assets/js/custom/jquery.growl.js"></script> 
  <script src="<?= base_url() ?>assets/js/custom/axios.min.js"></script> 
    <!--<script>-->
    <!--    axios.defaults.headers.common = {-->
    <!--        'X-Requested-With': 'XMLHttpRequest',-->
    <!--        'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content');-->
    <!--    };-->
    <!--</script>-->
</head>
<body class="sidebar-fixed">
  <div class="container-scroller">
    <!-- //navigation -->
    <?php $this->load->view('templates/backend/navigation'); ?>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       <!-- //sidebar -->
       <?php $this->load->view('templates/backend/sidebar'); ?>
       <div class="content-wrapper">
