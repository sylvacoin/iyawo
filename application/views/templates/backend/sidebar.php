 <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-category">
              <span class="nav-link">GENERAL</span>
            </li>
            <?php if(isset($menus) && count($menus) > 0 ): foreach( $menus as $menu ): ?>                
            <?php if ( count($menu['sub_menus']) > 0 ) : ?>
              <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#<?= isset($menu['id']) ? $menu['id']: 'menu_'.$menu['menu_id'] ?>" aria-expanded="false" aria-controls="<?= isset($menu['id']) ? $menu['id']: 'menu_'.$menu['menu_id'] ?>">
                  <span class="menu-title"><?= $menu['label'] ?></span>
                  <i class="<?= $menu['icon'] ?> menu-icon"></i>
                </a>
                <div class="collapse" id="<?= isset($menu['id']) ? $menu['id']: 'menu_'.$menu['menu_id'] ?>">
                  <ul class="nav flex-column sub-menu">
            <?php foreach( $menu['sub_menus'] as $k => $sub_menu): ?>
                    <li class="nav-item"> 
                      <a class="nav-link" href="<?= site_url($sub_menu['url'])  ?>"><?= $sub_menu['label'] ?></a>
                    </li>
            <?php endforeach; ?>
                  </ul>
                </div>
            </li>
            <?php else: ?>
              <li class="nav-item">
                <a class="nav-link" href="<?= site_url($menu['url'] ) ?>">
                  <span class="menu-title"><?= $menu['label'] ?></span>
                  <i class="<?= $menu['icon'] ?> menu-icon"></i>
                </a>
              </li>
            <?php endif; ?>
            <?php endforeach; endif; ?>
          </ul>
        </nav>
        <!-- partial -->
