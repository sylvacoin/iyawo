   </div>

   <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © <?= date('Y') ?> <a href="http://www.dimconnect.com/" target="_blank">Dimconnect</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">powered by Iyawo pastor</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- End plugin js for this page-->
  <script src="<?= base_url() ?>assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
 <script src="<?= base_url() ?>assets/vendor/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
  <!-- inject:js -->
  <script src="<?= base_url() ?>assets/js/off-canvas.js"></script>
  <script src="<?= base_url() ?>assets/js/hoverable-collapse.js"></script>
  <script src="<?= base_url() ?>assets/js/misc.js"></script>
  
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- <script src="<?= base_url() ?>assets/js/dashboard.js"></script> -->
  <!-- End custom js for this page-->
</body>

</html>
