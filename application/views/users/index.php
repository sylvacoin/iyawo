<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="table-responsive">
                <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config" :actions="actions" @on-add="addUser" @on-view="view">
                  <template slot-scope="props" slot="full_name">
                    <span>{{props.row.firstname}} {{props.row.lastname}}</span>
                  </template>
                  <template slot="option" slot-scope="props">
                    <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="edit(props.row)"
                    title="edit administrator">  <span class="icon-pencil"></span>
                    </a>

                    <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="goto(props.row)" title="view customers"><span class="icon-people"></span>
                    </a>

                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" @click="ondelete( props.row)" title="ban administrator "><span class="icon-ban"></span>
                    </a>
                  </template>
                </vue-bootstrap4-table> 
          </div>
        </div>
      </div>
    </div>


    <!-- The Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel-3"  v-if="!isEdit">Add Sub Administrator</h5>
            <h5 class="modal-title" id="exampleModalLabel-3" v-else>Edit Sub Administrator</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <form @submit.prevent="doSubmit">
              <div class="form-group row">
                <label for="fullname" class="col-4 col-form-label">First Name</label> 
                <div class="col-8">
                  <input id="fullname" v-model="Item.firstname" type="text" required="required" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label for="fullname" class="col-4 col-form-label">Last Name</label> 
                <div class="col-8">
                  <input id="fullname" v-model="Item.lastname" type="text" required="required" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label for="email" class="col-4 col-form-label">Email Address</label> 
                <div class="col-8">
                  <input id="email" v-model="Item.email"  type="text" class="form-control" required="required">
                </div>
              </div>
              <div class="form-group row">
                <label for="email" class="col-4 col-form-label">User Role</label> 
                <div class="col-8">
                  <select v-model="Item.user_group_id" id="" class="form-control">
                    <option :value="role.user_group_id" v-for="role in adminRoles">{{role.user_group}}</option>
                  </select>
                </div>
              </div>
              <div class="form-group row"  v-if="!isEdit">
                <label for="password" class="col-4 col-form-label">Password</label> 
                <div class="col-8">
                  <div class="input-group">
                    <input id="password" v-model="Item.password" type="text" required="required" class="form-control"> 
                    <input v-model="Item.role_id" type="hidden"> 
                    <div class="input-group-append">
                      <div class="input-group-button"><button class="btn btn-default btn-sm" type="button"   @click="generatePassword">generate</button></div>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="form-group row"  v-if="!isEdit">
                <div class="col-4"></div> 
                <div class="col-8">
                  <div class="form-group">
                    <div class="form-check  form-check-flat">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" v-model="Item.send_mail">
                        Send welcome mail to admin?
                      </label>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="form-group row">
                <div class="offset-4 col-8">
                  <button name="submit" type="submit" class="btn btn-primary" v-if="!isEdit" :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Create Sub Admin</button>
                  <button name="submit" type="submit" class="btn btn-primary" v-else :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Edit Admin</button>
                </div>
              </div>
            </form>

          </div>
          
        </div>
      </div>
    </div>
    <!-- Modal Ends -->
</div>



<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    //Start datatable
    rows: [],
    roles:[],
    columns: [
            {
              label: "Full Name",
              slot_name: "full_name",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Email",
              name: "email",
              sort: true
            },
            {
              label: "Options",
              name: "user_id",
              sort: false,
              slot_name: "option"
            }
          ],
    actions: [
      {
        btn_text: "Add Admin",
        event_name: "on-add",
        class: "btn btn-primary",
        event_payload: {
          
        }
      },
      {
        btn_text: "Flagged Admins",
        event_name: "on-view",
        class: "btn btn-primary",
        event_payload: {
          
        }
      }
    ],
    config: {
      pagination: false,
        pagination_info: false,
        num_of_visibile_pagination_buttons: 7,
        per_page: 10,
        checkbox_rows: false,
        highlight_row_hover: false,
        rows_selectable: false,
        show_refresh_button: false,
        show_reset_button: false,
        card_mode: false,
        per_page_options: [5, 10, 20, 30],
        multi_column_sort:false,
        preservePageOnDataChange: true,
        row_text_alignment: "text-left",
        column_text_alignment: "text-left",
        global_search: {
            visibility: true
        }
    },
    classes: {
      table: "table-bordered table-striped table-condensed"
    },
    //end datatable
    Item: {
      firstname: '',
      lastname: '',
      email:'',
      password: '',
      user_group_id: 0,
      send_mail: true
    },
    editedIndex: -1,
    isLoading: false,
    isEdit: false
  },
  mounted(){
    this.init();
    //this.getDefaultRole('sub administrators');
    this.getRoles();
  },
  computed:{
    
      adminRoles: function(){
        if ( this.roles.length > 0 )
        {
         return this.roles.filter((role) => {
          return role.account_type == 'admin' && role.user_group_id > 1
          });
        }
      
    }
  },
  methods: {
    init(){
      axios.get(`${base_url}users/get-users`).then((response) => {
        if (response.data.status == 200 )
        {
          this.rows = response.data.data;
        }
      });
    },
  	getRoles(){
  		axios.get(`${base_url}user-groups/get-user-groups`).then((response) => {
        if (response.status == 200 )
        {
          this.roles = response.data.data;
        }
      });
  	},
    addUser(payload) {
      this.isEdit = false;
      this.Item = {
                    firstname: '',
                    lastname: '',
                    email:'',
                    password: '',
                    user_group_id: 0,
                    send_mail: true
                  };
      $('#modal').modal('show');
    },

    doSubmit(){
      this.isLoading = true;
      let formData = new FormData;
      formData.append('firstname', this.Item.firstname);
      formData.append('lastname', this.Item.lastname);
      formData.append('email', this.Item.email);
      formData.append('role_id', this.Item.user_group_id);
      if ( !this.isEdit )
      {
        formData.append('password', this.Item.password);
        formData.append('send_mail', this.Item.send_mail);
      }
      
      if ( this.Item.user_id > 0 )
      {
        axios.post(`${base_url}users/update-user/${this.Item.user_id}`, formData).then((response) => {
          console.log(response);
          if ( response.data.status == 200 )
          {
             $.growl.notice({message:response.data.message});
              Object.assign(
                this.rows[this.editedIndex],
                response.data.data
              )
            $('#modal').modal('hide');
          }
          if ( response.data.status == 300 )
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }

          if ( response.data.status == 400 )
          {
            $.growl.error({ message: response.data.message });
          }
          this.isLoading = false;
        }).catch((e) =>
        {
          console.log(e);
        });
      }else{
        axios.post(base_url+'users/create-user', formData).then((response) => {
          console.log(response);
          if ( response.data.status == 200)
          {
            $.growl.notice({message:response.data.message});
            this.rows.push(response.data.data);
            $('#modal').modal('hide');
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400 )
          {
            $.growl.error({ message: response.data.message });
          }
          this.isLoading = false;
        }).catch((e) =>
        {
          this.isLoading = false;
          console.log(e);
        });   
      }
    },

    edit(item) {
      
      this.editedIndex = this.rows.findIndex(function(el){
        return el.user_id == item.user_id
      });
      console.log(this.editedIndex);
      this.Item = Object.assign({}, item)
      this.isEdit = true
      $('#modal').modal('show');
    },

    generatePassword() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!%&^,";

      for (var i = 0; i < 8; i++)
      {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      this.Item.password = text;
    },

    ondelete(item) {
       axios.post(`${base_url}users/flag-user/${item.user_id}`).then((response) => {
          if ( response.data.status == 200 )
          {
              $.growl.notice({ message: response.data.message });
              this.editedIndex = this.rows.findIndex(function(el){
                return el.user_id == item.user_id
              });
              this.rows.splice(this.editedIndex, 1);
          }
        }).catch(e => console.log(e));
    },

    goto(item)
    {
      window.location.href = `${base_url}customers/handler/${item.user_id}/view-customer`;x
    },

    view()
    {
      window.location.href = `${base_url}users/flagged`;
    }

  }
})
</script>
