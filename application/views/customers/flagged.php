<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="table-responsive">
                <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config" :actions="actions" @on-add="goback">
                  <template slot="option" slot-scope="props">
                      <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="unflag(props.row)"><span class="icon-reload"></span></a>

                      <a href="javascript:void(0);" class="btn btn-danger btn-sm" @click="ondelete( props.row)"><span class="icon-trash"></span></a>
                  </template>
                </vue-bootstrap4-table> 
          </div>
        </div>
      </div>
    </div>
</div>



<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    //Start datatable
    rows: [],
    roles:[],
    columns: [
            {
              label: "Full Name",
              name: "full_name",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Customer No",
              name: "customer_no",
              sort: true
            },
            {
              label: "Options",
              name: "customer_id",
              sort: false,
              slot_name: "option"
            }
          ],
    actions: [
      {
        btn_text: "View customers",
        event_name: "on-add",
        class: "btn btn-primary",
        event_payload: {
          
        }
      }
    ],
    config: {
      pagination: false,
        pagination_info: false,
        num_of_visibile_pagination_buttons: 7,
        per_page: 10,
        checkbox_rows: false,
        highlight_row_hover: false,
        rows_selectable: false,
        show_refresh_button: false,
        show_reset_button: false,
        card_mode: false,
        per_page_options: [5, 10, 20, 30],
        multi_column_sort:false,
        preservePageOnDataChange: true,
        row_text_alignment: "text-left",
        column_text_alignment: "text-left",
        global_search: {
            visibility: true
        }
    },
    classes: {
      table: "table-bordered table-striped table-condensed"
    },
    //end datatable
    Item: {
      firstname: '',
      lastname: '',
      email:'',
      password: '',
      customer_group_id: 0,
      send_mail: true
    },
    editedIndex: -1,
    isLoading: false,
    isEdit: false
  },
  mounted(){
    this.init();
    //this.getDefaultRole('sub administrators');
    this.getRoles();
  },
  methods: {
    init(){
      axios.get(`${base_url}customers/get-flagged-customers`).then((response) => {
        if (response.data.status == 200 )
        {
          this.rows = response.data.data;
        }
      });
    },
    unflag(item) {
      
      axios.post(`${base_url}customers/unflag-customer/${item.customer_id}`).then((response) => {
          if ( response.data.status == 200 )
          {
              $.growl.notice({ message: response.data.message });
              this.editedIndex = this.rows.findIndex(function(el){
                return el.customer_id == item.customer_id
              });
              this.rows.splice(this.editedIndex, 1);
          }
        }).catch(e => console.log(e));
    },

    ondelete(item) {
       var decision = confirm('Are you sure you want to delete this customer? This process is irreversible!');
       if ( decision )
       {
          axios.post(`${base_url}customers/delete-customer/${item.customer_id}`).then((response) => {
            if ( response.data.status == 200 )
            {
                $.growl.notice({ message: response.data.message });
                this.editedIndex = this.rows.findIndex(function(el){
                  return el.customer_id == item.customer_id
                });
                this.rows.splice(this.editedIndex, 1);
            }
          }).catch(e => console.log(e));
       }
    },

    goback(){
      window.history.back();
    }

  }
})
</script>
