<div id="app">
  <div class="row user-profile">
    <div class="col-lg-4 side-left d-flex align-items-start">
      <div class="row">
        <div class="col-lg-12" style="margin-bottom: 10px;">
          <div class="card">
            <div class="card-body avatar">
              <h4 class="card-title">Info</h4>
              <p class="name"><?=isset($title) ? $title : ''?></p>
              <a class="d-block text-center text-dark" href="#">( <?= !empty($phone) ? $phone : 'No phone number' ?> )</a>
            </div>
          </div>
        </div>
        <div class="col-lg-12 " style="margin-bottom: 10px;">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title mb-0">Original Balance</h4>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                      <div class="d-flex">
                        <h2 class="mb-0"><?= DEFAULT_CURRENCY ?> {{formatPrice(obalance)}}</h2>
                      </div>
                    </div>
                    <div class="d-inline-block">
                      <div class="bg-success px-4 py-2 rounded">
                        <i class="icon-wallet text-white icon-lg"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <div class="col-lg-12" style="margin-bottom: 10px;">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title mb-0">Withdrawable Balance</h4>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                      <div class="d-flex">
                        <h2 class="mb-0"><?= DEFAULT_CURRENCY ?> {{formatPrice(balance)}}</h2>
                      </div>
                    </div>
                    <div class="d-inline-block">
                      <div class="bg-success px-4 py-2 rounded">
                        <i class="icon-wallet text-white icon-lg"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <div class="col-lg-12" style="margin-bottom: 10px;">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title mb-0">Total Cards</h4>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                      <div class="d-flex">
                        <h2 class="mb-0"> {{card_count}}</h2>
                      </div>
                    </div>
                    <div class="d-inline-block">
                      <div class="bg-success px-4 py-2 rounded">
                        <i class="mdi mdi-buffer text-white icon-lg"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div>
    <div class="col-lg-8 side-right stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
            <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Info</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" @click="createCard">Add Card</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" @click="quickDeposit">Quick Deposit</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" @click="makeWithdrawal">Withdraw</a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)" @click="viewHistory" class="btn btn-">History</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="security-tab" href="javascript:void(0);" role="tab" aria-controls="security" @click="goback()"><i class="fa fa-arrow-left"></i> Back</a>
              </li>
            </ul>
          </div>
          <div class="wrapper">
            <hr>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">
                <div class="table-responsive">
                  <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config">
                  <template slot="card_no" slot-scope="props">
                    <a href="" :href="`${base_url}transactions/view/${encodeURI(props.row.card_id)}`">
                      {{props.row.card_no}}
                    </a>
                  </template>
                  <template slot="total" slot-scope="props">
                      <?= DEFAULT_CURRENCY ?>{{formatPrice(props.row.card_wbalance)}}
                  </template>
                  <template slot="trans_badge" slot-scope="props">
                    <span class="badge badge-success" v-if="props.row.trans_type == 'credit'">{{props.row.trans_type}}</span>
                    <span class="badge badge-danger" v-else>{{props.row.trans_type}}</span>
                  </template>
                  <template slot="status" slot-scope="props">
                    <span class="badge badge-danger" v-if="props.row.status == 'withdrawn'">{{props.row.status}}</span>
                    <span class="badge badge-warning" v-else-if="props.row.status == 'pending withdrawal'">{{props.row.status}}</span>
                    <span class="badge badge-success" v-else>{{props.row.status}}</span>
                  </template>
                  <template slot="options" slot-scope="props">
                    <button class="btn btn-xs btn-primary" @click="doResetCard(props.row)" :disabled="props.row.total <= 0.00" v-if="uac">Reset </button>
                  </template>
                </vue-bootstrap4-table>
                </div>
              </div><!-- tab content ends -->
              <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
        
  <!-- The Card Modal -->
  <div class="modal fade" id="cardModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">New Card</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form @submit.prevent="doCreateCard">
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Card No.</label>
              <div class="col-8">
                <input id="cardno" v-model=" Item.card_no " type="text" required="required" class="form-control">
              </div>
            </div>
            <div class="form-group row ">
              <label for="start" class="col-4 col-form-label">Start Count</label>
              <div class="col-8">
                <input id="card_start" v-model="Item.card_start" type="number" min="1" max="30" required="required" class="form-control">
              </div>
            </div>
            <div class="form-group row ">
              <label for="start" class="col-4 col-form-label">Start Amount/Rate (<?= DEFAULT_CURRENCY ?>)</label>
              <div class="col-8">
                <input id="start_amount" v-model="Item.start_amount" type="number" min="100" required="required" class="form-control">
              </div>
            </div>

            <div class="form-group row">
              <div class="offset-4 col-8">
                <button type="submit" class="btn btn-primary" :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Create Card</button>
              </div>

            </div>
          </form>

        </div>

      </div>
    </div>
  </div>
<!-- Modal Ends -->
 <!-- The Quick Withdrawal Modal -->
  <div class="modal fade" id="WithdrawalModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Quick withdrawal request</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form @submit.prevent="doSubmit">
           
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Card</label>
              <div class="col-8">
                <select v-model="quick.card_no" id="" class="form-control" @change="getBalance()">
                  <option value="" selected disabled>select</option>
                  <option :value="c.card_id" v-for="c in rows">{{c.card_no}}</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Amount</label>
              <div class="col-8">
                <input id="amount" v-model="quick.amount" type="number" min="500" required="required" class="form-control" readonly>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-4 col-8">
                <button type="submit" class="btn btn-primary" :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Request withdrawal</button>
              </div>

            </div>
          </form>

        </div>

      </div>
    </div>
  </div>
<!-- Modal Ends -->
 <!-- The Quick deposit Modal -->
  <div class="modal fade" id="QuickDepositModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Quick Deposit</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form @submit.prevent="doSubmit">
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Card no</label>
              <div class="col-8">
                <select v-model="quick.card_no" id="" class="form-control" required="required"@change="getStartAmount()">
                  <option value="" selected disabled>select</option>
                  <option :value="c.card_id" v-for="c in rows">{{c.card_no}}</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Amount</label>
              <div class="col-8">
                <input id="amount" v-model="quick.amount " type="number" min="100" required="required" class="form-control" readonly="readonly" @change="calcPayment()">
              </div>
            </div>
            <div class="form-group row">
              <label for="no_days" class="col-4 col-form-label">No. of days</label>
              <div class="col-8">
                <input id="no_days" v-model="quick.no_days" type="number" min="1" required="required" class="form-control" @change="calcPayment()">
              </div>
            </div>
            <div class="form-group row">
              <label for="no_days" class="col-4 col-form-label">Amount to pay (<?= DEFAULT_CURRENCY ?>)</label>
              <div class="col-8">
                <input type="text" v-model="calcAmt" readonly="readonly" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-4 col-8">
                <button type="submit" class="btn btn-primary" v-if=" Item.transtype == 'credit'" :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Make contribution</button>
                <button type="submit" class="btn btn-primary" v-else :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Make deposit</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
<!-- Modal Ends -->
</div>

<script type="text/javascript">
  var app  = new Vue({
    el:'#app',
    data: {
      base_url:base_url,
      calcAmt:0,
      isLoading : false,
      balance: 0,
      obalance: 0,
      card_count: 0,
      total_contributions: 0,
      uac : false,
      Item: {
        user: <?= $this->uri->segment(3);?>,
        card_start: 1,
        card_no:''
      },
      quick: {
        card_no:'',
        amount: 0,
        trans_type:'credit',
        no_days:1
      },
      rows:[],
      columns: [
        {
          label: "Card No",
          slot_name: "card_no",
          sort: true,
          row_text_alignment: "text-left",
          column_text_alignment: "text-left",
        },
        {
          label: "Total Deposits",
          slot_name: "total",
          sort: true,
          row_text_alignment: "text-left",
          column_text_alignment: "text-left",
        },
        {
          label: "Status",
          slot_name: "status",
          sort: true,
          row_text_alignment: "text-center",
          column_text_alignment: "text-center",
        },
        {
          label: "Last Transaction Date",
          name: "updated_at",
          sort: true,
        },
        {
          label: "Options",
          slot_name: "options"
        },
        
      ],
      config: {
        pagination: true,
        pagination_info: false,
        num_of_visibile_pagination_buttons: 7,
        show_reset_button:false,
        show_refresh_button:false,
        per_page: 10,
        checkbox_rows: false,
        highlight_row_hover: false,
        rows_selectable: false,
        multi_column_sort: false,
        // highlight_row_hover_color:"grey",
        card_title: "",
        card_mode: false,
        selected_rows_info:true,
         per_page_options: [5, 10, 20, 30],
      },

    },
    created(){
      this.init();
      this.getAccountBalance();
      this.verifyAccess()
    },
    methods: {
      //get transactions for this user,
      init()
      {
        axios.get(`${base_url}cards/get-cards/${this.Item.user}`).then((response) => {
          if ( response.data.status == 200 )
          {
             this.rows = response.data.data.cards;
             this.card_count = response.data.data.count;
             
          }
        });
      },
      verifyAccess()
      {
        axios.get(`${base_url}users/is-admin`).then((response) => {
          if ( response.data.status = 200 )
          {
            this.uac = response.data.data.is_admin;
          }
        });
      },
      //get account balance for this user
      getAccountBalance()
      {
        axios.get(base_url+'transactions/getAllUserBalance/<?=$this->uri->segment(3)?>').then((response) => {
          if ( response.status == 200 )
          {
             this.obalance = response.data.data.original;
             this.balance = response.data.data.withdrawable;
          }
        });
      },
      //add contribution
      createCard(){
        this.quick =  {
          card_no:'',
          amount: 0,
          card_start:1,
          start_amount: 500
        };
        $('#cardModal').modal('show');
      },
      //add contribution
      quickDeposit(){
        this.quick =  {
          card_no:'',
          amount: 0,
          trans_type:'credit',
          no_days:1
        };
        $('#QuickDepositModal').modal('show');
      },

      //make withdrawal
      makeWithdrawal(){
        this.quick =  {
          card_no:'',
          amount: 0,
          trans_type:'debit'
        };
        $('#WithdrawalModal').modal('show');
      },
      doResetCard(card){
        let form  = new FormData();
        form.append('card_id', card.card_id);

        axios.post(base_url+'transactions/reset-card-transaction', form).then((response) => {
          
          if ( response.data.status == 200)
          {
            this.getAccountBalance();
            this.init();
            $.growl.notice({message:response.data.message});
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400)
          {
            $.growl.error({ message: response.data.message});
          }
          this.isLoading = false;
        }).catch((e)=>{
          
          $.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });
      },
      doSubmit() 
      {
        this.isLoading = true;
        let form  = new FormData();
        form.append('user_id', this.Item.user);
        form.append('amount', this.quick.amount);
        form.append('card_no', this.quick.card_no);
        form.append('no_days', this.quick.no_days);
        form.append('trans_type', this.quick.trans_type);
        axios.post(base_url+'transactions/create-transaction', form).then((response) => {
          
          if ( response.data.status == 200)
          {
            this.getAccountBalance();
            this.init();
            $.growl.notice({message:response.data.message});
            this.calcAmt = 0;
            $('#QuickDepositModal').modal('hide');
            $('#WithdrawalModal').modal('hide');
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400)
          {
            $.growl.error({ message: response.data.message});
          }
          this.isLoading = false;
        }).catch((e)=>{
          
          $.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });

      },

      doCreateCard()
      {
        this.isLoading = true;
        let form  = new FormData();
        form.append('user_id', this.Item.user);
        form.append('card_no', this.Item.card_no);
        form.append('start_amount', this.Item.start_amount);
        form.append('card_start', this.Item.card_start);
        axios.post(`${base_url}cards/create-card`, form).then((response) => {
          console.log(response);
          if ( response.data.status == 200)
          {
            $.growl.notice({message:response.data.message});
            this.rows.push(response.data.data);
            $('#cardModal').modal('hide');
          }
          if ( response.data.status == 300 )
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          this.isLoading = false;
        }).catch((e)=>{
          console.log(e);
          //$.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });

      },

      formatPrice(value)
      {
        return formatCurrency(value);
      },

      viewHistory(){
        window.location.href = `${base_url}transactions/history/${this.Item.user}`;
      }

      ,
      goback(){
        window.history.back();
      },

      getBalance()
      {
        let app = this;
        let card = this.rows.filter(function(item){
          return item.card_id == app.quick.card_no;
        });
        console.log(card);
        if ( card )
        {
          this.quick.amount = card[0].card_wbalance;
        }else{
          this.quick.amount = 0;
        }
        
      },
      calcPayment()
      {
        let bal = this.quick.amount * this.quick.no_days;
        this.calcAmt = this.formatPrice(bal);
      },
      getStartAmount()
      {
        let app = this;
        let card = this.rows.filter(function(item){
          return item.card_id == app.quick.card_no;
        });
        console.log(card);
        if ( card )
        {
          this.quick.amount = card[0].start_amount;
        }else{
          this.quick.amount = 0;
        }
        
      }

    },
  })
</script>
