<div class="row" id="app">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-dark text-left p-5">
                <h2>Login to <?= SITENAME ?></h2>
                <form class="pt-5" @submit.prevent="doAjaxLogin()" ref="form">
                  
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input type="email" class="form-control" v-model="email" placeholder="Username" required>
                      <i class="mdi mdi-account"></i>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" v-model="password" placeholder="Password" required>
                      <i class="mdi mdi-eye"></i>
                    </div>
                    <div class="mt-5">
                      <button type="submit" class="btn btn-block btn-warning btn-lg font-weight-medium" :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Login</button>
                    </div>
                    <div class="mt-3 text-center">
                      <a href="<?= site_url('forgot-password') ?>" class="auth-link text-white">Forgot password?</a>
                    </div>
                                  
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
<script type="text/javascript">
    var app = new Vue({
        el:'#app',
        data: {
            email:'',
            password: '',
            isLoading: false,
        },
        methods: {

            doAjaxLogin(){
                this.isLoading = true;
                let formData = new FormData;
                formData.append('email', this.email);
                formData.append('password', this.password);
                axios.post(`${base_url}auth/do-login`, formData).then((response) => {

                    if ( response.data.status == 200 )
                    {
                        this.isLoading = false;
                        $.growl.notice({message: response.data.message});
                        window.location.href= response.data.url;
                    }

                    if ( response.data.status == 400 )
                    {
                        this.isLoading = false;
                        $.growl.error({message: response.data.message});
                    }
                }).catch((errors) =>{
                    $.growl.error({message: "Invalid username or password" });
                    this.isLoading = false;
                });
            }
        }
    })
</script>
