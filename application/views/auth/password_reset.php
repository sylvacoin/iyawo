<div class="row" id="app">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-dark text-left p-5">
                <h2>Reset Password</h2>
                
                <form class="pt-5" @submit.prevent="doAjaxResetPassword()" ref="form">
                  
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email Address</label>
                      <input type="email" class="form-control" v-model="email" placeholder="" required>
                      <i class="mdi mdi-email"></i>
                    </div>

                    <div class="mt-5">
                      <button type="submit" class="btn btn-block btn-warning btn-lg font-weight-medium" :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Reset password</button>
                    </div>
                    <div class="mt-3 text-center">
                      <a href="<?= site_url('login') ?>" class="auth-link text-white">Back to login</a>
                    </div>
                                  
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
<script type="text/javascript">
    var app = new Vue({
        el:'#app',
        data: {
            email:'',
            isLoading: false,
        },
        methods: {

            doAjaxResetPassword(){
                this.isLoading = true;
                let formData = new FormData;
                formData.append('email', this.email);
                axios.post(base_url+'api/reset-password', formData).then((response) => {

                    if ( response.status == 200 )
                    {
                        $.growl.notice({message: 'Password reset was successful. Please Check your email for new password.'});
                    }
                    this.isLoading = false;
                }).catch((errors) =>{
                    $.growl.error({message: "Email address is not yet registered on this platform" });
                    this.isLoading = false;
                });
            }
        }
    })
</script>
