<div class="row" id="app">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-dark text-left p-5">
                <h2>Reset Password</h2>
                
                <form class="pt-5" @submit.prevent="doAjaxResetPassword()" ref="form">
                    <input type="hidden" value="<?= isset($token)?$token:'' ?>" ref="token">
                    <div class="form-group">
                      <label for="exampleInputEmail1">New Password</label>
                      <input type="password" class="form-control" v-model="password" placeholder="" required>
                      <i class="mdi mdi-lock"></i>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Confirm Password</label>
                      <input type="password" class="form-control" v-model="confirm_password" placeholder="" required>
                      <i class="mdi mdi-lock"></i>
                    </div>

                    <div class="mt-5">
                      <button type="submit" class="btn btn-block btn-warning btn-lg font-weight-medium" :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Reset password</button>
                    </div>
                    <div class="mt-3 text-center">
                      <a href="<?= site_url('login') ?>" class="auth-link text-white">Back to login</a>
                    </div>
                                  
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
<script type="text/javascript">
    var app = new Vue({
        el:'#app',
        data: {
            password:'',
            confirm_password:'',
            token: '',
            isLoading: false,

        },
        methods: {

            doAjaxResetPassword(){
                this.isLoading = true;

                if ( this.password != this.confirm_password )
                {
                  this.isLoading = false;
                  $.growl.error({message: 'Password and Confirm Password dont match ' });
                  return false;
                }

                let formData = new FormData;
                formData.append('password', this.password);
                formData.append('confirm_password', this.confirm_password);
                formData.append('token', this.$refs.token.value);
                axios.post(`${base_url}api/reset-password-complete`, formData).then((response) => {

                    if ( response.status == 200 )
                    {
                        $.growl.notice({message: 'Password reset was successful. Redirecting to login...'});
                        setTimeout(function() {
                          window.location.href = `${base_url}login`;
                        }, 3000);
                    }
                    this.isLoading = false;
                }).catch((errors) =>{
                    $.growl.error({message: "An error occured please contact admin" });
                    this.isLoading = false;
                });
            }
        }
    })
</script>
