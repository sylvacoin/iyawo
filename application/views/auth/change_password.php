<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="row">
            <div class="col-sm-6 col-xs-12 offset-3">
               <form @submit.prevent="doAjaxChangePassword()">
                 <div class="form-group row">
                  <label class="col-sm-12 col-form-label">New password</label>
                  <div class="col-sm-12">
                    <input type="password" class="form-control" v-model="password" placeholder="New password" />
                  </div>
                </div>
             
                <div class="form-group row">
                  <label class="col-sm-12 col-form-label">Confirm new password</label>
                  <div class="col-sm-12">
                    <input type="password" class="form-control" v-model="confirm_password" placeholder="confirm password" />
                  </div>
                </div>
             
              
                <button type="submit" class="btn btn-success mr-1" :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Change password</button>
               </form>
            </div>
          </div>
           
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
  var app = new Vue({
    el:'#app',
    data: {
      password:'',
      confirm_password:'',
      isLoading: false,
    },
    methods: {
      doAjaxChangePassword(){
        this.isLoading = true;
        let formdata = new FormData;
        formdata.append('password', this.password);
        formdata.append('confirm_password', this.confirm_password);
        axios.post(base_url+'api/change-password', formdata).then((response) => {
          if ( response.status == 200 )
          {
            this.isLoading = false;
            $.growl.notice({message: 'Password change was successful.'});
          }
        }).catch((error) => {
          this.isLoading = false;
          $.growl.error({message: 'Password change request was not successful. Passwords dont match.'});
        })
      }
    }
  })
</script>
