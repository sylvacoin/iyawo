<div class="row" id="app">
  <div class="col-lg-12 grid-margin">
    <div class="card overflow-hidden">
      <div class="card-body mx-3">
        <h2 class="card-title border-bottom-none"><?=isset($title) ? $title : ""?></h2>
          <nested-draggable :tasks="list">
          </nested-draggable>
          <div class="row justify-content-center">
            <a class="btn btn-primary mr-2" href="javascript:void(0)" @click="doSave">Save</a>
            <a class="btn btn-danger ml-2" href="javascript:void(0)" @click="doReset">Reset</a>
          </div>
      </div>
    </div>
    </div>
    <div class="col-lg-6 grid-margin">

    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
<script src="<?=base_url()?>assets/js/custom/nested-vuedraggable.js"></script>

<script type="text/javascript" >

  var app = new Vue({
    el:'#app',
    data: {

      list: [],
      drag: false,
    },

    created(){
      this.init();
    },

    computed: {
      orderedList() {
        return JSON.stringify(this.list, null, 2);
      }
    },

    methods: {
      sort() {
        this.list = this.list.sort((a, b) => a.order - b.order);
      },
      removeAt(idx) {
        this.list.splice(idx, 1);
      },
      init(){
        axios.get(base_url + 'menu/get_json').then((response) => {
          if (response.status == 200 )
          {
            this.list = response.data.data;
          }
        })
      },
      doSave()
      {
        axios.post(base_url + 'menu/Order', JSON.stringify(this.orderedList)).then((response) => {
          console.log(response);
            if ( response.status == 200 && typeof response.data.data != undefined)
            {
              $.growl.notice({message: 'Menu order was saved successfully'});
              window.location.reload();
            }else{
              $.growl.error({message: 'An error occured'});
            }
        });
      },
      doReset()
      {
        this.init();
      }
    }
  })
</script>

<style>
  /*.dragArea, .dragArea ul {
    border: 1px dashed #444;
    min-height: 100px;
  }*/

  li .draggable:hover {
    cursor: move;
  }
</style>
