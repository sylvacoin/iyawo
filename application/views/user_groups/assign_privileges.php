<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden">
        <div class="card-body mx-3">
          <div class="d-flex justify-content-between">
            <h2 class="card-title border-bottom-none">Assign privileges to {{Item.user_group}}</h2>
            <i class="icon-arrow-left" @click="goback()" style="cursor:pointer;"></i>
          </div>
          <div class="row">
              <div class="col-sm-12">
                <div class="list-wrapper">
                  <ul class="d-flex flex-column todo-list">
                    <li v-for="row in rows">
                      <div class="form-group">
                        <label class="form-check-label">
                          <input class="checkbox" type="checkbox" v-model="selected[row.menu_id]" name="selected[]">
                            {{row.label}}
                        </label>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>
          </div>

          <div class="row mt-3 mb-4 justify-content-center">
            <button class="btn btn-primary mr-2" @click="doSubmit()">Save</button><button class="btn btn-danger ml-2">Reset</button>
          </div>
        </div>
      </div>


    </div>
  </div>

<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    rows:[],
    selectedRows:[],
    selected: [],
    Item: {
      user_group: '',
      is_default: false,
      user_group_id: <?=$this->uri->segment(3)?>
    },
    editedIndex: -1,
    isLoading: false,
    isEdit: false
  },
  mounted(){
    this.init();
  },
  methods: {
    init(){
      axios.get(`${base_url}menu/get-all`).then((response) => {
        if (response.status == 200 )
        {
          this.rows = response.data.data;
        }
      }).then(()=>{
        axios.get(`${base_url}user-groups/get-user-group-menus/${this.Item.user_group_id}`).then((response) => {
          if (response.status == 200 )
          {
            this.selected = response.data.data.selected;
            this.Item.user_group = response.data.data.user_group;
          }
        })
      });
    },

    doSubmit(){
      this.isLoading = true;

        axios.post(base_url+'user-groups/create-user-group-menus/'+this.Item.user_group_id, this.selected).then((response) => {
          console.log(response);
          if ( response.status == 200 && response.data.data != undefined)
          {
            console.log(this.rows, this.editedIndex);
             $.growl.notice({message:"User group was updated successfully"});
            window.location.reload();
          }
          if ( response.data.errors != undefined )
          {
            let errors = response.data.errors;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          this.isLoading = false;
        }).catch((e) =>
        {
          console.log(e);
        });

    },

    goback(){
      return window.history.back();
    }
  }
})
</script>
