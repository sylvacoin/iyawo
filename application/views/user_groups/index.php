<div class="row" id="app">
    <div class="col-lg-4 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?=isset($title) ? $title : ""?></h2>
            <form @submit.prevent="doSubmit">
            <div class="form-group row">
              <label class="col-sm-12 col-form-label">User group</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" v-model="Item.user_group" value=""/>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-12 col-form-label">Account Type</label>
              <div class="col-sm-12">
                <select class="form-control" v-model="Item.access_type">
                  <option value="admin">Administrator</option>
                  <option value="guest" selected>Guest</option>
                </select>
              </div>
            </div>

            <div class="form-check">
                <label class="form-check-label">
                  <input type="checkbox" class="form-check-input form-control" v-model="Item.is_default">
                  set as default user group
                </label>
            </div>

            <button type="submit" class="btn btn-success mr-1">
              <span v-if="isEdit">Update</span>
              <span v-else>Add Group</span>
            </button>
            <button class="btn btn-light" @click="reset" v-if="isEdit">Cancel</button>
            </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8 grid-margin">
      <div class="card overflow-hidden">
        <div class="card-body mx-3">
            <div class="table-responsive">
    					<vue-bootstrap4-table :rows="rows" :columns="columns" :config="config">
    						<template slot="default_status" slot-scope="props">
    						  <span class="badge badge-success" v-if="props.row.is_default == 1"> Default </span>
                  <span v-else></span>
    						</template>
                <template slot="option" slot-scope="props">
                      <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="edit(props.row)" title="Edit" v-if="props.row.can_edit == 1"><span class="icon-pencil"></span></a>
                      <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="modify( props.row)" title="set privilege"><span class="icon-lock"></span></a>
                      <a href="javascript:void(0);" class="btn btn-danger btn-sm" @click="ondelete( props.row)" v-if="props.row.can_edit == 1"><span class="icon-trash"></span></a>
                </template>
    					</vue-bootstrap4-table>
            </div>

          </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    //Start datatable
    rows: [],
    columns: [
            {
              label: "Group Name",
              name: "user_group",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Default status",
              name: "is_default",
              sort: true,
              slot_name: 'default_status'
            },
            {
              label: "Options",
              name: "user_group_id",
              sort: false,
              slot_name: "option"
            }
        ],
    config: {
        pagination: false,
        pagination_info: false,
        num_of_visibile_pagination_buttons: 7,
        per_page: 10,
        checkbox_rows: false,
        highlight_row_hover: false,
        rows_selectable: false,
        show_refresh_button: false,
        show_reset_button: false,
        card_mode: false,
        per_page_options: [5, 10, 20, 30],
        multi_column_sort:false,
        preservePageOnDataChange: true,
        row_text_alignment: "text-left",
        column_text_alignment: "text-left",
        global_search: {
            visibility: false
        }
        
      },
    classes: {
      table: "table-bordered table-striped table-condensed"
    },
    //end datatable
    Item: {
      user_group: '',
      access_type: 'admin',
      is_default: false,
      user_group_id: 0
    },
    editedIndex: -1,
    isLoading: false,
    isEdit: false
  },
  mounted(){
    this.init();
  },
  methods: {
    init(){
      axios.get(base_url+'user-groups/get-user-groups').then((response) => {
        if (response.status == 200 )
        {
          this.rows = response.data.data;
        }
      });
    },
    doSubmit(){
      this.isLoading = true;
      let formData = new FormData;
      formData.append('user_group', this.Item.user_group);
      formData.append('access_type', this.Item.access_type);
      formData.append('is_default', this.Item.is_default);

      if ( this.Item.user_group_id > 0 )
      {
        axios.post(`${base_url}user-groups/put-user-group/${ this.Item.user_group_id}`, formData).then((response) => {
          console.log(response);
          if ( response.data.status == 200)
          {
            console.log(this.rows, this.editedIndex);
             $.growl.notice({message:response.data.message});
             //reset all default to false on client
             if ( this.Item.is_default )
             {
                let idx = this.rows.findIndex(function(el){
                  return el.is_default == true;
                });
                if ( idx > -1 )
                {
                  this.rows[idx].is_default = false;
                }

             }
             if ( response.data.status == 400 )
              {
                $.growl.error({ message: response.data.message });
              }
              Object.assign(
                this.rows[this.editedIndex],
                response.data.data
              )
            $('#modal').modal('hide');
          }
          if ( response.data.status == 300 )
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          this.isLoading = false;
        }).catch((e) =>
        {
          console.log(e);
        });
      }else{
        axios.post(`${base_url}user-groups/post-user-group`, formData).then((response) => {

          if ( response.data.status == 300 )
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }

          if ( response.data.status == 400 )
          {
            $.growl.error({ message: response.data.message });
          }

          if ( response.status == 201)
          {
            $.growl.notice({message: response.data.message});
            //reset all default to false on client
             if ( this.Item.is_default )
             {
                let idx = this.rows.findIndex(function(el){
                  return el.is_default == true;
                });
                if ( idx > -1 )
                {
                  this.rows[idx].is_default = false;
                }
                
             }
            this.rows.push(response.data.data);
            $('#modal').modal('hide');
          }

          this.isLoading = false;
        }).catch((e) =>
        {
          console.log(e);
        });
      }
    },

    edit(item) {
      this.editedIndex = this.rows.findIndex(function(el){
        return el.user_group_id == item.user_group_id;
      });
      console.log(this.editedIndex);
      this.Item             = Object.assign({}, item);
      this.Item.access_type = this.Item.account_type;
      this.Item.is_default  = this.Item.is_default == true ? 1 : 0;
    },

    ondelete(item) {
      axios.post(`${base_url}user-groups/delete-user-group/${item.user_group_id}`).then((response) => {

          if ( response.data.status == 300 )
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }

          if ( response.data.status == 200 )
          {
            $.growl.notice({message:response.data.message});
            let idx = this.rows.findIndex(function(el){
              return el.user_group_id == item.user_group_id;
            });
            this.rows.splice(idx, 1 );
          }

          this.isLoading = false;
        }).catch((e) =>
        {
          console.log(e);
        });
    },

    reset()
    {
      this.isEdit = false;
      this.Item = {
                    user_group: '',
                    is_default: false
                  };
    },

    modify(item)
    {
      window.location.href=base_url+"user-groups/assign-privileges/"+item.user_group_id
    }

  }
})
</script>
