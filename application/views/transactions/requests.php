<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="table-responsive">
                <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config">
                  <template slot="amount" slot-scope="props">
                    <?= DEFAULT_CURRENCY ?>{{formatPrice(props.row.amount)}}
                  </template>
                  <template slot="type" slot-scope="props">
                    <span class="badge badge-danger" v-if="props.row.trans_type == 'debit'"> {{props.row.trans_type}} </span>
                    <span class="badge badge-success" v-else> {{props.row.trans_type}} </span>
                  </template>
                  <template slot="option" slot-scope="props">
                      <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="markAsCompleted(props.row)"><span class="icon-check"></span></a>
                  </template>
                </vue-bootstrap4-table> 
          </div>
      </div>
    </div>
  </div>



<!-- Modal Ends -->
</div>



<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    //Start datatable
    rows: [],
    columns: [
            {
              label: "Date",
              name: "trans_date",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Customer",
              name: "full_name",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Transaction type",
              name: "trans_type",
              sort: true,
              slot_name: "type"
            },
            {
              label: "Amount",
              name: "amount",
              sort: true,
              row_text_alignment: 'text-right',
              column_text_alignment: 'text-right'
            },
            {
              label: "Options",
              name: "user_id",
              sort: false,
              slot_name: "option"
            }
          ],
    actions: [],
    config: {
      pagination: true,
      pagination_info: false,
      num_of_visibile_pagination_buttons: 7,
      per_page: 10,
      checkbox_rows: false,
      highlight_row_hover: false,
      rows_selectable: false,
      multi_column_sort: true,
      show_reset_button:false,
      show_refresh_button:false,      

      // highlight_row_hover_color:"grey",
      // card_title: "Vue Bootsrap 4 advanced table",
      card_mode: false,
      selected_rows_info:false,
      per_page_options: [5, 10, 20, 30],
      row_text_alignment: "text-left",
      column_text_alignment: "text-left",
    },
    classes: {
      table: "table-bordered table-striped table-condensed"
    },
    //end datatable
    editedIndex: -1,
    isLoading: false,
    isEdit: false
  },
  mounted(){
    this.init();
  },
  methods: {
    init(){
      axios.get(base_url+'transactions/getAjaxAllPendingWithdrawal').then((response) => {
        if (response.status == 200 )
        {
          this.rows = response.data.data;
        }
      });
    },

    markAsCompleted(item) {
      axios.get(base_url+'transactions/markAsCompleted/'+item.transaction_id).then((response) => {
        if (response.status == 200 )
        {
          this.editedIndex = this.rows.findIndex(function(el){
            return el.user_id == item.user_id
          });
          this.rows.splice(this.editedIndex, 1);
          $.growl.notice({message:"Transaction was marked as completed"});
        }
      });
    },
    formatPrice(value)
    {
      return formatCurrency(value);
    }
  }
})
</script>
