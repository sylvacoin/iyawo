<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="table-responsive">
                <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config" :actions="actions" @on-view="goback">
                  <template slot="amount" slot-scope="props">
                    <?= DEFAULT_CURRENCY ?>{{ formatPrice(props.row.amount) }}
                  </template>
                  <template slot="status" slot-scope="props">
                    <span class="badge badge-warning badge-sm" v-if="props.row.trans_status == 'pending'"> {{props.row.trans_status}} </span>
                    <span class="badge badge-success badge-sm" v-else> {{props.row.trans_status}} </span>
                  </template>
                  <template slot="trans_badge" slot-scope="props">
                    <span class="badge badge-success" v-if="props.row.trans_type == 'credit'">{{props.row.trans_type}}</span>
                    <span class="badge badge-danger" v-else>{{props.row.trans_type}}</span>
                  </template>
                  <template slot="option" slot-scope="props" v-if="isAdmin">
                      <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="edit(props.row)"><span class="icon-pencil"></span></a>

                      <a href="javascript:void(0);" class="btn btn-danger btn-sm" @click="ondelete( props.user_id)"><span class="icon-trash"></span></a>
                  </template>
                </vue-bootstrap4-table> 
          </div>
      </div>
    </div>
  </div>

</div>



<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    //Start datatable
    rows: [],
    columns: [
            {
              label: "Card",
              name: "card_no",
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Date",
              name: "trans_date",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Amount",
              slot_name: "amount",
              sort: true,
              row_text_alignment: 'text-right',
              column_text_alignment: 'text-right'
            },
            {
              label: "Type",
              slot_name: "trans_badge",
              name:'trans_type',
              sort: true,
              row_text_alignment: 'text-right',
              column_text_alignment: 'text-right'
            },
            {
              label: "Status",
              name: "trans_status",
              sort: true,
              row_text_alignment: 'text-right',
              column_text_alignment: 'text-right'
            }
          ],
    actions: [
      {
        btn_text: "Back",
        event_name: "on-view",
        class: "btn btn-primary",
        event_payload: {
          
        }
      }
    ],
    config: {
      pagination: true,
      pagination_info: false,
      num_of_visibile_pagination_buttons: 7,
      per_page: 10,
      checkbox_rows: false,
      highlight_row_hover: false,
      rows_selectable: false,
      multi_column_sort: false,
      show_refresh_button: false,
      show_reset_button: false,
      card_mode: false,
      selected_rows_info:true,
      per_page_options: [5, 10, 20, 30],
      row_text_alignment: "text-left",
      column_text_alignment: "text-left",
    },
    classes: {
      table: "table-bordered table-striped table-condensed"
    },
    //end datatable
    card_no: '<?= $this->uri->segment(3); ?>',
    editedIndex: -1,
    isLoading: false,
    isEdit: false,
    isAdmin:false
  },
  mounted(){
    this.init();
  },
  methods: {
    init(){
      axios.get(`${base_url}transactions/get-transaction-history/${this.card_no}`).then((response) => {
        if (response.data.status == 200 )
        {
          this.rows = response.data.data;
        }
      });
    },

    edit(item) {
      
      this.editedIndex = this.rows.findIndex(function(el){
        return el.user_id == item.user_id
      });
      console.log(this.editedIndex);
      this.Item = Object.assign({}, item);
      this.isEdit = true;
      $('#modal').modal('show');
    },

    formatPrice(item)
    {
      return formatCurrency(item);
    },

    goback(){
      window.history.back();
    }
  }
})
</script>
