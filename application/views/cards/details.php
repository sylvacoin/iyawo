<div id="app">
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <h2 class="card-title border-bottom-none"><?=isset($title) ? $title : ''?></h2>
        <div class="card-body mx-3">
          <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config">
            <template slot="trans_badge" slot-scope="props">
              <span class="badge badge-success" v-if="props.row.trans_type == 'credit'">{{props.row.trans_type}}</span>
              <span class="badge badge-danger" v-else>{{props.row.trans_type}}</span>
            </template>
          </vue-bootstrap4-table>
        </div>
      </div>
    </div>
  </div>
  <!-- The Modal -->
  <div class="modal fade" id="cardModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">New Card</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form @submit.prevent="doCreateCard">
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Card No.</label>
              <div class="col-8">
                <input id="cardno" v-model=" Item.card_no " type="text" required="required" class="form-control">
              </div>
            </div>
            <div class="form-group row ">
              <label for="start" class="col-4 col-form-label">Start Count</label>
              <div class="col-8">
                <input id="card_start" v-model="Item.start_from" type="number" min="1" max="30" required="required" class="form-control">
              </div>
            </div>

            <div class="form-group row">
              <div class="offset-4 col-8">
                <button type="submit" class="btn btn-primary" :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Create Card</button>
              </div>

            </div>
          </form>

        </div>

      </div>
    </div>
  </div>
<!-- Modal Ends -->
 <!-- The Modal -->
  <div class="modal fade" id="modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" v-if=" Item.transtype == 'credit' ">Quick Deposit</h5>
          <h5 class="modal-title" v-else>Make withdrawal request</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form @submit.prevent="doSubmit">
            <div class="form-group row">
              <label for="amount" class="col-4 col-form-label">Amount</label>
              <div class="col-8">
                <input id="amount" v-model=" Item.amount " type="text" required="required" class="form-control">
              </div>
            </div>

            <div class="form-group row">
              <div class="offset-4 col-8">
                <button type="submit" class="btn btn-primary" v-if=" Item.transtype == 'credit'" :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Make contribution</button>
                <button type="submit" class="btn btn-primary" v-else :disabled="isLoading" ><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Request withdrawal</button>
              </div>

            </div>
          </form>

        </div>

      </div>
    </div>
  </div>
<!-- Modal Ends -->
</div>

<script type="text/javascript">
  var app  = new Vue({
    el:'#app',
    data: {
      isLoading : false,
      balance: 0,
      total_contributions: 0,
      Item: {
        user: <?=$this->uri->segment(3);?>,
        transtype: "credit",
        amount: 0,
      },
      rows:[],
      columns: [
        {
          label: "Transaction Date",
          name: "trans_date",
          sort: true,
          row_text_alignment: "text-left",
          column_text_alignment: "text-left",
        },
        {
          label: "Amount",
          name: "amount",
          sort: true,
        },
        {
          label: "Transaction",
          name: "trans_type",
          sort: true,
          slot_name: "trans_badge"
        },
        {
          label: "Status",
          name: "trans_status",
          sort: true,
          row_text_alignment: "text-left",
          column_text_alignment: "text-left",
        },

      ],
      config: {
        pagination: true,
        pagination_info: false,
        num_of_visibile_pagination_buttons: 7,
        show_reset_button:false,
        show_refresh_button:false,
        per_page: 10,
        checkbox_rows: false,
        highlight_row_hover: false,
        rows_selectable: false,
        multi_column_sort: false,
        // highlight_row_hover_color:"grey",
        card_title: "",
        card_mode: false,
        selected_rows_info:true,
         per_page_options: [5, 10, 20, 30],
      },

    },
    created(){
      this.init();
      this.getAccountBalance();
    },
    methods: {
      //get transactions for this user,
      init()
      {
        axios.get(`${base_url}cards/get-cards/${this.Item.user}`).then((response) => {
          if ( response.data.status == 200 )
          {
             this.rows = response.data.data;
          }
        });
      },
      //get account balance for this user
      getAccountBalance()
      {
        axios.get(base_url+'transactions/getUserBalance/<?=$this->uri->segment(3)?>').then((response) => {
          if ( response.status == 200 )
          {
             this.balance = response.data.data;
          }
        });
      },
      //add contribution
      createCard(){
        $('#cardModal').modal('show');
      },
      //add contribution
      addContribution(){
        this.Item.transtype = 'credit';
        $('#modal').modal('show');
      },

      //make withdrawal
      makeWithdrawal(){
        this.Item.transtype = 'debit';
        $('#modal').modal('show');
      },

      doSubmit()
      {
        this.isLoading = true;
        let form  = new FormData();
        form.append('user_id', this.Item.user);
        form.append('amount', this.Item.amount);
        form.append('trans_type', this.Item.transtype);
        axios.post(base_url+'transactions/json_create', form).then((response) => {
          console.log(response);
          if ( response.status == 201)
          {
            this.getAccountBalance();
            $.growl.notice({message:"Request was successful"});
            this.rows.push(response.data.data);
            $('#modal').modal('hide');
          }
          if ( response.status == 200 && typeof response.data.errors != undefined )
          {
            let errors = response.data.errors;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          this.isLoading = false;
        }).catch((e)=>{
          console.log(e);
          //$.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });

      },

      doCreateCard()
      {
        this.isLoading = true;
        let form  = new FormData();
        form.append('user_id', this.Item.user);
        form.append('card_no', this.Item.card_no);
        form.append('card_start', this.Item.card_start);
        axios.post(`${base_url}cards/post-Card`, form).then((response) => {
          console.log(response);
          if ( response.data.status == 200)
          {
            this.getAccountBalance();
            $.growl.notice({message:response.data.message});
            this.rows.push(response.data.data);
            $('#modal').modal('hide');
          }
          if ( response.status == 200 && typeof response.data.errors != undefined )
          {
            let errors = response.data.errors;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          this.isLoading = false;
        }).catch((e)=>{
          console.log(e);
          //$.growl.error({ message: "An error occured while making request" });
          this.isLoading = false;
        });

      },



    },
  })
</script>
