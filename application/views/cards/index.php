<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="table-responsive">
                <vue-bootstrap4-table :rows="rows" :columns="columns" :config="config" :actions="actions" @on-add="addUser" @on-view="view">
                  <template slot="option" slot-scope="props">
                      <a href="javascript:void(0);" class="btn btn-primary btn-sm" @click="edit(props.row)"><span class="icon-pencil"></span></a>

                      <a href="javascript:void(0);" class="btn btn-danger btn-sm" @click="ondelete( props.row)"><span class="icon-trash"></span></a>
                  </template>
                </vue-bootstrap4-table> 
          </div>
        </div>
      </div>
    </div>


    <!-- The Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel-3"  v-if="!isEdit">Add customer</h5>
            <h5 class="modal-title" id="exampleModalLabel-3" v-else>Edit Customer</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <form @submit.prevent="doSubmit">
              <div class="form-group row">
                <label for="fullname" class="col-4 col-form-label">Full Name</label> 
                <div class="col-8">
                  <input id="fullname" v-model="Item.full_name" type="text" required="required" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label for="customer_no" class="col-4 col-form-label">Customer No</label> 
                <div class="col-8">
                  <input id="customer_no" v-model="Item.customer_no" type="text" required="required" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label for="email" class="col-4 col-form-label">Gender</label> 
                <div class="col-8">
                  <select v-model="Item.gender" id="" class="form-control">
                    <option value="" disabled selected>select</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                </div>
              </div>
           
              <div class="form-group row">
                <div class="offset-4 col-8">
                  <button name="submit" type="submit" class="btn btn-primary" v-if="!isEdit" :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Create Customer</button>
                  <button name="submit" type="submit" class="btn btn-primary" v-else :disabled="isLoading"><i class="fa fa-spinner fa-spin" v-if="isLoading"></i> Edit Customer</button>
                </div>
              </div>
            </form>

          </div>
          
        </div>
      </div>
    </div>
    <!-- Modal Ends -->
</div>



<script type="text/javascript">
var app = new Vue({
  el:'#app',
  data: {
    //Start datatable
    rows: [],
    roles:[],
    columns: [
            {
              label: "Full Name",
              name: "full_name",
              sort: true,
              row_text_alignment: 'text-left',
              column_text_alignment: 'text-left'
            },
            {
              label: "Customer No",
              name: "customer_no",
              sort: true
            },
            {
              label: "Gender",
              name: "gender",
              sort: true
            },
            {
              label: "Options",
              name: "customer_id",
              sort: false,
              slot_name: "option"
            }
          ],
    actions: [
      {
        btn_text: "Add customer",
        event_name: "on-add",
        class: "btn btn-primary",
        event_payload: {
          
        }
      },
      {
        btn_text: "Flagged customers",
        event_name: "on-view",
        class: "btn btn-primary",
        event_payload: {
          
        }
      }
    ],
    config: {
      pagination: true,
        pagination_info: false,
        num_of_visibile_pagination_buttons: 7,
        per_page: 10,
        checkbox_rows: false,
        highlight_row_hover: false,
        rows_selectable: false,
        show_refresh_button: false,
        show_reset_button: false,
        card_mode: false,
        per_page_options: [5, 10, 20, 30],
        multi_column_sort:false,
        preservePageOnDataChange: true,
        row_text_alignment: "text-left",
        column_text_alignment: "text-left",
        global_search: {
            visibility: true
        }
    },
    classes: {
      table: "table-bordered table-striped table-condensed"
    },
    //end datatable
    Item: {
      full_name: '',
      customer_no: '',
      gender:''
    },
    editedIndex: -1,
    isLoading: false,
    isEdit: false
  },
  mounted(){
    this.init();
  },
  methods: {
    init(){
      axios.get(`${base_url}customers/get-customers`).then((response) => {
        if (response.data.status == 200 )
        {
          this.rows = response.data.data;
        }
      });
    },
    addUser(payload) {
      this.isEdit = false;
      this.Item = {
                    full_name: '',
                    customer_no: '',
                    gender:''
                  };
      $('#modal').modal('show');
    },

    doSubmit(){
      this.isLoading = true;
      let formData = new FormData;
      formData.append('full_name', this.Item.full_name);
      formData.append('customer_no', this.Item.customer_no);
      formData.append('gender', this.Item.gender);
     
      if ( this.Item.customer_id > 0 )
      {
        axios.post(`${base_url}customers/update-customer/${this.Item.customer_id}`, formData).then((response) => {
          console.log(response);
          if ( response.data.status == 200 )
          {
             $.growl.notice({message:response.data.message});
              Object.assign(
                this.rows[this.editedIndex],
                response.data.data
              )
            $('#modal').modal('hide');
          }
          if ( response.data.status == 300 )
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }

          if ( response.data.status == 400 )
          {
            $.growl.error({ message: response.data.message });
          }
          this.isLoading = false;
        }).catch((e) =>
        {
          console.log(e);
        });
      }else{
        axios.post(base_url+'customers/create-customer', formData).then((response) => {
          console.log(response);
          if ( response.data.status == 200)
          {
            $.growl.notice({message:response.data.message});
            this.rows.push(response.data.data);
            $('#modal').modal('hide');
          }
          if ( response.data.status == 300)
          {
            let errors = response.data.message;
             for(val in errors)
             {
                $.growl.error({ message: errors[val] });
             }
          }
          if ( response.data.status == 400 )
          {
            $.growl.error({ message: response.data.message });
          }
          this.isLoading = false;
        }).catch((e) =>
        {
          this.isLoading = false;
          console.log(e);
        });   
      }
    },

    edit(item) {
      
      this.editedIndex = this.rows.findIndex(function(el){
        return el.customer_id == item.customer_id
      });
      console.log(this.editedIndex);
      this.Item = Object.assign({}, item)
      this.isEdit = true
      $('#modal').modal('show');
    },

    generatePassword() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!%&^,";

      for (var i = 0; i < 8; i++)
      {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      this.Item.password = text;
    },

    ondelete(item) {
      axios.post(`${base_url}customers/flag-customer/${item.customer_id}`).then((response) => {
          if ( response.data.status == 200 )
          {
              $.growl.notice({ message: response.data.message });
              this.editedIndex = this.rows.findIndex(function(el){
                return el.user_id == item.user_id
              });
              this.rows.splice(this.editedIndex, 1);
          }
        }).catch(e => console.log(e));
    },

    view(){
      window.location.href = `${base_url}customers/flagged`;
    }

  }
})
</script>
