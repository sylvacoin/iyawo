<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <div class="table-responsive">
                <table class="table table-striped table-sm card-text table-bordered">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Message</th>
                         <th>Action </th>
                         <th>Options</th>
                       </tr>
                     </thead>
                     <tbody>

                    <?php if ( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach( $data->result() as $row): ?>
                      <tr>
                         <th scope="row"><?= $i++ ?></th>
                         <td> <?= isset($row->body)?$row->body:'' ?></td>
                         <td><?= isset($row->action)?$row->action:'' ?></td>
                         <td>
                             <?= anchor('sms/modify/'.$row->sms_id, 'modify', 'class="btn btn-success btn-xs"') ?>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                          <th scope="row" colspan="4">
                              <p> No sms template has been added</p>
                          </th>
                        </tr>
                   <?php endif; ?>
                     </tbody>
                   </table>
          </div>
        </div>
      </div>
    </div>
</div>