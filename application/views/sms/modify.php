<link href="<?= base_url() ?>assets/css/summernote.min.css" rel="stylesheet">
<!-- <link href="<?= base_url() ?>assets/css/summernote-theme.css" rel="stylesheet"> -->
<div class="row" id="app">
    <div class="col-lg-12 grid-margin">
      <div class="card overflow-hidden dashboard-curved-chart">
        <div class="card-body mx-3">
          <h2 class="card-title border-bottom-none"><?= isset($title)?$title:"" ?></h2>
          <?= validation_errors('<div class="alert alert-danger">','</div>') ?>
          <?= form_open('sms/submit/'.$this->uri->segment(3), 'class="form-horizontal"') ?>
              
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="body" id="summernote"><?= isset($data->body)?$data->body:'' ?></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 form-control-label">Key word mappings</label>
                <div class="col-md-9">
                  <textarea name="keyword_mapping" rows="8" cols="80" class="form-control" disabled><?= isset($data->keyword_mapping) ?$data->keyword_mapping:'' ?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary center-block">Save changes</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/custom/summernote.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('#summernote').summernote({
  height: 400
});

});
</script>
