<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->model(['Mdl_transactions','Mdl_customers', 'mdl_cards']);
		///is_user_logged();
	}

	public function index() {
	    is_user_logged();
		$this->render->view('transactions/index.php', 'All Transactions', null, 'backend');
	}

	public function handler_transactions() {
	    is_user_logged();
		$this->render->view('transactions/handler_transactions.php', 'My Transactions', null, 'backend');
	}
	
	public function requests() {
	    is_user_logged();
		$this->render->view('transactions/requests.php', 'Withdrawal Requests', null, 'backend');
	}

	public function view( $card_id = null ) {
	    is_user_logged();
	    if ( !is_numeric($card_id))
	    {
	    	redirect('notFound');
	    } 

	    $card = $this->mdl_cards->get_where($card_id);
	    if ( !empty($card) )
	    {
	    	$card_title = $card->row()->card_no;
	    }else{
	    	redirect('notFound');
	    }

		$this->render->view('transactions/view.php', '('.$card_title.') Transaction Records', null, 'backend');
	}

	public function history($customer_id) {
	    is_user_logged();
		$this->render->view('transactions/history.php', 'Transaction history', null, 'backend');
	}

	public function details($transaction_id) {
	    is_user_logged();
		$user = $this->Mdl_transactions->get_where($transaction_id);
		if ($user->num_rows() > 0) {
			$this->render->view('transactions/details.php', $user->row()->full_name);
		} else {
			$this->render->view('errors/404.php', 'Record cannot be found');
		}
	}

	public function getAjaxTransactionStatistics($value='')
	{
		$role = get_session_data('role_id');
		if ( is_admin($role) )
		{
			return $this->getAjaxTransactionStats();
		}else{
			return $this->getAjaxHandlerTransactionStats();
		}
	}

	public function getAjaxTransactionStats()
	{
		$this->load->model(['mdl_cards','mdl_customers']);
		//todays transactions
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$data['todaysTransactions'] = $this->Mdl_transactions->getToday()->result_array();

		//todays transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$data['todaysTotal'] = $this->Mdl_transactions->getToday()->row()->total;

		//weekly transactions
		$this->db->select('sum( amount ) as total, WEEK( trans_date ) as date');
		$this->db->group_by('WEEK(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$data['weeklyTransactions'] = $this->Mdl_transactions->getThisWeek()->result_array();

		//weekly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$data['weeklyTotal'] = $this->Mdl_transactions->getThisWeek()->row()->total;

		//monthly transactions
		$this->db->select('sum( amount ) as total, MONTHNAME( trans_date ) as date');
		$this->db->group_by('MONTHNAME(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$data['monthlyTransactions'] = $this->Mdl_transactions->getThisMonth()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$data['monthlyTotal'] = $this->Mdl_transactions->getThisMonth()->row()->total;


		//monthly transactions
		$this->db->select('sum( amount ) as total, YEAR( trans_date ) as date');
		$this->db->group_by('YEAR(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$data['yearlyTransactions'] = $this->Mdl_transactions->getThisYear()->result_array();

		//monthly transactions total
		$this->db->select_sum('amount','total');
		$this->db->where('trans_type', 'credit');
		$data['yearlyTotal'] = $this->Mdl_transactions->getThisYear()->row()->total;

		$data['totalProfit'] =  $this->Mdl_transactions->get_total_profit();
		$data['totalWithdrawalBalance'] = $this->mdl_cards->get_pending_withdrawals();
		$data['PendingWithdrawalCount'] = $this->mdl_cards->get_pending_withdrawal_count();
		$data['totalBalance'] = $this->Mdl_customers->get_total_balance();
		$data['dailyProfit'] = $this->Mdl_transactions->get_daily_profit();
		$data['dailyTotal'] = $this->Mdl_transactions->get_daily_total();
		$data['monthlyProfit'] = $this->Mdl_transactions->get_monthly_profit();


		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxHandlerTransactionStats()
	{
		$handler_id = get_session_data('ssid');
		$this->load->model(['mdl_cards','mdl_customers']);
		//todays transactions
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('transaction_by_id', $handler_id);

		$data['todaysTransactions'] = $this->Mdl_transactions->getToday()->result_array();

		//todays transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['todaysTotal'] = $this->Mdl_transactions->getToday()->row()->total;

		//weekly transactions
		$this->db->select('sum( amount ) as total, WEEK( trans_date ) as date');
		$this->db->group_by('WEEK(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['weeklyTransactions'] = $this->Mdl_transactions->getThisWeek()->result_array();

		//weekly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['weeklyTotal'] = $this->Mdl_transactions->getThisWeek()->row()->total;

		//monthly transactions
		$this->db->select('sum( amount ) as total, MONTHNAME( trans_date ) as date');
		$this->db->group_by('MONTHNAME(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['monthlyTransactions'] = $this->Mdl_transactions->getThisMonth()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['monthlyTotal'] = $this->Mdl_transactions->getThisMonth()->row()->total;


		//monthly transactions
		$this->db->select('sum( amount ) as total, YEAR( trans_date ) as date');
		$this->db->group_by('YEAR(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['yearlyTransactions'] = $this->Mdl_transactions->getThisYear()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('is_flagged', '0');
		$this->db->where('trans_type', 'credit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['yearlyTotal'] = $this->Mdl_transactions->getThisYear()->row()->total;

		$data['totalProfit'] =  $this->Mdl_transactions->get_handler_total_profit();
		$data['dailyProfit'] =  $this->Mdl_transactions->get_handler_daily_profit();
		$data['monthlyProfit'] =  $this->Mdl_transactions->get_handler_monthly_profit();
		$data['totalBalance'] = $this->Mdl_customers->get_handler_total_balance();

		return $this->render->json(['data' => $data], 200);

	}

	public function getAjaxHandlerClientStats( $account_id )
	{
		//todays transactions
		$data['todaysTransactions'] = $this->Mdl_transactions->getToday()->result_array();

		//todays transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['todaysTotal'] = $this->Mdl_transactions->getToday()->row()->total;

		//weekly transactions
		$this->db->select('sum( amount ) as total, WEEK( trans_date ) as date');
		$this->db->group_by('WEEK(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['weeklyTransactions'] = $this->Mdl_transactions->getThisWeek()->result_array();

		//weekly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['weeklyTotal'] = $this->Mdl_transactions->getThisWeek()->row()->total;

		//monthly transactions
		$this->db->select('sum( amount ) as total, MONTHNAME( trans_date ) as date');
		$this->db->group_by('MONTHNAME(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['monthlyTransactions'] = $this->Mdl_transactions->getThisMonth()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['monthlyTotal'] = $this->Mdl_transactions->getThisMonth()->row()->total;


		//monthly transactions
		$this->db->select('sum( amount ) as total, YEAR( trans_date ) as date');
		$this->db->group_by('YEAR(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['yearlyTransactions'] = $this->Mdl_transactions->getThisYear()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'credit');
		$this->db->where('account_id', $account_id);
		$data['yearlyTotal'] = $this->Mdl_transactions->getThisYear()->row()->total;

		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxWithdrawalStats()
	{
		//todays transactions
		$data['todaysTransactions'] = $this->Mdl_transactions->getToday()->result_array();

		//todays transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$data['todaysTotal'] = $this->Mdl_transactions->getToday()->row()->total;

		//weekly transactions
		$this->db->select('sum( amount ) as total, WEEK( trans_date ) as date');
		$this->db->group_by('WEEK(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$data['weeklyTransactions'] = $this->Mdl_transactions->getThisWeek()->result_array();

		//weekly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$data['weeklyTotal'] = $this->Mdl_transactions->getThisWeek()->row()->total;

		//monthly transactions
		$this->db->select('sum( amount ) as total, MONTHNAME( trans_date ) as date');
		$this->db->group_by('MONTHNAME(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$data['monthlyTransactions'] = $this->Mdl_transactions->getThisMonth()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$data['monthlyTotal'] = $this->Mdl_transactions->getThisMonth()->row()->total;


		//monthly transactions
		$this->db->select('sum( amount ) as total, YEAR( trans_date ) as date');
		$this->db->group_by('YEAR(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$data['yearlyTransactions'] = $this->Mdl_transactions->getThisYear()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$data['yearlyTotal'] = $this->Mdl_transactions->getThisYear()->row()->total;

		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxHandlerWithdrawalStats( $handler_id )
	{
		//todays transactions
		$data['todaysTransactions'] = $this->Mdl_transactions->getToday()->result_array();

		//todays transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['todaysTotal'] = $this->Mdl_transactions->getToday()->row()->total;

		//weekly transactions
		$this->db->select('sum( amount ) as total, WEEK( trans_date ) as date');
		$this->db->group_by('WEEK(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['weeklyTransactions'] = $this->Mdl_transactions->getThisWeek()->result_array();

		//weekly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['weeklyTotal'] = $this->Mdl_transactions->getThisWeek()->row()->total;

		//monthly transactions
		$this->db->select('sum( amount ) as total, MONTHNAME( trans_date ) as date');
		$this->db->group_by('MONTHNAME(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['monthlyTransactions'] = $this->Mdl_transactions->getThisMonth()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['monthlyTotal'] = $this->Mdl_transactions->getThisMonth()->row()->total;


		//monthly transactions
		$this->db->select('sum( amount ) as total, YEAR( trans_date ) as date');
		$this->db->group_by('YEAR(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['yearlyTransactions'] = $this->Mdl_transactions->getThisYear()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('transaction_by_id', $handler_id);
		$data['yearlyTotal'] = $this->Mdl_transactions->getThisYear()->row()->total;

		return $this->render->json(['data' => $data], 200);

	}

	public function getAjaxClientWithdrawalStats( $account_id )
	{
		//todays transactions
		$data['todaysTransactions'] = $this->Mdl_transactions->getToday()->result_array();

		//todays transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['todaysTotal'] = $this->Mdl_transactions->getToday()->row()->total;

		//weekly transactions
		$this->db->select('sum( amount ) as total, WEEK( trans_date ) as date');
		$this->db->group_by('WEEK(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['weeklyTransactions'] = $this->Mdl_transactions->getThisWeek()->result_array();

		//weekly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['weeklyTotal'] = $this->Mdl_transactions->getThisWeek()->row()->total;

		//monthly transactions
		$this->db->select('sum( amount ) as total, MONTHNAME( trans_date ) as date');
		$this->db->group_by('MONTHNAME(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['monthlyTransactions'] = $this->Mdl_transactions->getThisMonth()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['monthlyTotal'] = $this->Mdl_transactions->getThisMonth()->row()->total;


		//monthly transactions
		$this->db->select('sum( amount ) as total, YEAR( trans_date ) as date');
		$this->db->group_by('YEAR(trans_date)');
		$this->db->order_by('trans_date');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['yearlyTransactions'] = $this->Mdl_transactions->getThisYear()->result_array();

		//monthly transactions total
		$this->db->select('sum( amount ) as total');
		$this->db->where('trans_type', 'debit');
		$this->db->where('account_id', $account_id);
		$data['yearlyTotal'] = $this->Mdl_transactions->getThisYear()->row()->total;

		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTotalTodaysTransactions() {
		$this->db->select('sum( amount )');
		$data = $this->Mdl_transactions->getToday();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTodaysTransactions() {
		$data = $this->Mdl_transactions->getToday();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTotalWeeklyTransactions() {
		$this->db->select('sum( amount )');
		$data = $this->Mdl_transactions->getThisWeek();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxWeeklyTransactions() {
		$data = $this->Mdl_transactions->getThisWeek();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTotalMonthlyTransactions() {
		$this->db->select('sum( amount )');
		$data = $this->Mdl_transactions->getThisMonth();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxMonthlyTransactions() {
		$data = $this->Mdl_transactions->getThisMonth();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTotalProfit() {
		$data = $this->Mdl_transactions->get_total_profit();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTotalBalance() {
		$data = $this->Mdl_transactions->get_total_balance();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTotalPendingWithdrawal() {
		$data = $this->Mdl_transactions->get_total_withdrawal();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxTransactions($id) {
		$data = $this->Mdl_transactions->get_where_custom(['clients.client_id' => $id])->result();
		return $this->render->json(['data' => $data], 200);
	}

	public function json_get_all_transactions() {
		$this->db->join('customers', 'customers.customer_id = transactions.customer_id');
		$this->db->join('cards', 'cards.card_id = transactions.card_id', 'left');
		$data = $this->Mdl_transactions->get()->result();
		return $this->render->json(['data' => $data], 200);
	}

	public function json_get_handler_transactions() {
		is_user_logged();
		$handler_id = get_session_data('ssid');
		$this->db->where(['transaction_by_id' => $handler_id]);
		$this->db->join('customers', 'customers.customer_id = transactions.customer_id');
		$this->db->join('cards', 'cards.card_id = transactions.card_id', 'left');
		$data = $this->Mdl_transactions->get()->result();
		return $this->render->json(['data' => $data], 200);
	}

	public function json_get_transactions($type, $count = 5) {
		$this->db->where('trans_type', $type);
		$this->db->limit($count);

		$this->db->join('customers', 'customers.customer_id = transactions.customer_id');
		$this->db->join('users', 'users.user_id = transactions.transaction_by_id');
		$this->db->join('cards', 'cards.card_id = transactions.card_id', 'left');
		$this->db->where('customers.is_flagged', '0');
		$data = $this->Mdl_transactions->get()->result();
		return $this->render->json(['data' => $data], 200);
	}

	public function getAjaxAllPendingWithdrawal()
	{
		$this->db->join('customers c','c.customer_id = transactions.customer_id');
		$data = $this->Mdl_transactions->get_where_custom(['trans_type' => 'debit', 'trans_status' => 'pending'])->result();
		return $this->render->json(['data' => $data], 200);
	}

	public function markAsCompleted($id)
	{
		$this->Mdl_transactions->_update($id, ['trans_status' => 'completed']);
		$trans = $this->Mdl_transactions->get_where($id);
		$card = $this->mdl_cards->get_where($trans->row()->card_id)->row();
		$this->mdl_cards->_update($card->card_id, ['status' => 'withdrawn', 'card_wbalance' => 0]);
		return $this->render->json(['data' => 'success'], 200);
	}

	public function json_post_transaction()
	{
		$data = $this->get_data_from_post();
		if ($data['trans_type'] == 'credit')
		{
			return $this->json_post_credit_transaction();
		}
		return $this->json_post_debit_transaction();
	}

	public function json_post_debit_transaction() {
		$this->form_validation->set_rules('amount', 'Amount', 'required');
		$this->form_validation->set_rules('user_id', 'Customer', 'required');
		$this->form_validation->set_rules('trans_type', 'Transaction type', 'required');
		$transType = 'Withdrawal';

		if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();
			//$this->debugger->debug($data);

			$user_bal = $this->Mdl_customers->getUserBalance($data['user_id']);
			$data['amount'] = $data['amount'];
			if ($user_bal < $data['amount']) {
				return $this->render->json([
					'data' => [],
					'status'=>400,
					'message'=> 'Insufficient funds'
				], 200);
			}
			
			if (!$this->Mdl_transactions->isTransactionExist(
				date("Y-m-d"),
				$data['handler_id'],
				$data['user_id'],
				$data['card_id'],
				$data['trans_type'],
				$data['amount']
			)) {

                $this->db->trans_start();
				$result = $this->Mdl_transactions->_insert_debit($data);

				$data['transaction_id'] = $this->db->insert_id();
				$data['trans_date'] = "Just now";
				$data['trans_status'] = $data['trans_type'] == 'credit' ? 'completed' :'pending';
				$this->update_dr_card_balance($data['card_id']);
				$this->db->trans_complete();

//TODO: send an sms notification to client here

				return $this->render->json([
					'data' => $data,
					'status'=>200,
					'message'=> "{$transType} was made successfully"
				], 200);
				
			}
			return $this->render->json([
				'data' => [],
				'status'=>400,
				'message'=> 'Duplicate transaction'
			], 200);
		}

		return $this->render->json([
			'data' => [],
			'status'=>300,
			'message'=> $this->form_validation->error_array()
		], 200);
	}

	public function json_post_credit_transaction() {
		$this->form_validation->set_rules('amount', 'Amount', 'required');
		$this->form_validation->set_rules('user_id', 'Customer', 'required');
		$this->form_validation->set_rules('trans_type', 'Transaction type', 'required');
		$transType = 'Deposit';

		if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();
		
			$card_count = $this->Mdl_transactions->get_last_cardCount($data['card_id']);
			$no_days = $data['no_days'];
			$card_id = $data['card_id'];
			$user_id = $data['user_id'];
			$amount  = $data['amount'];

			//$this->debugger->debug($card_count);
			if ( $card_count - 1 + $no_days > 31 && $data['trans_type'] != 'debit')
			{
				$nn = $card_count - 1;
				return $this->render->json([
					'data' => $data,
					'status'=>400,
					'message'=> "Total days entered exceeds 31 days. Please try with something lesser. Current donation times is $nn. 
						No of days entered is $no_days"
				], 200);
			}

			if ( $data['amount'] == 0 || $no_days == 0 )
			{
				return $this->render->json([
						'data' => $data,
						'status'=>400,
						'message'=> "Please input a valid amount and valid no. of days"
					], 200);
			}

			if (!$this->Mdl_transactions->isTransactionExist(
				date("Y-m-d"),
				$data['handler_id'],
				$data['user_id'],
				$data['card_id'],
				$data['trans_type'],
				$data['amount']
			)) {
				
				unset($data['no_days']);
				$batch_data = array_fill(0, $no_days, $data);
				$this->db->trans_start();
				$result = $this->Mdl_transactions->_insert_credit($batch_data, $no_days);
				$data['transaction_id'] = $this->db->insert_id();
				$data['trans_date'] = "Just now";
				$data['trans_status'] = $data['trans_type'] == 'credit' ? 'completed' :'pending';
				$this->update_card_balance($data['card_id']);
				
				$this->db->trans_complete();
				//TODO: send an sms notification to client here
				//get customer phone number
				$this->load->model('mdl_customers');
				$customer_data = $this->mdl_customers->get_where_custom(['customer_id' => $user_id]);
				//$this->debugger->debug($customer_data->num_rows());
				if ( $customer_data->num_rows() > 0 )
				{
					$cd = $customer_data->row();
					if ( $cd->has_alert )
					{
						$card = $this->mdl_cards->get_where_custom(['card_id' => $card_id])->row();
						$total = $amount * $no_days;
						//send sms
$message = "
CARD NO:   $card->card_no
TOTAL BAL: $cd->wbalance
CARD BAL:  $card->card_wbalance 
DEP AMT:   $total
DATE: $card->updated_at
";						
						$this->load->library('sms');
						$this->sms->send_sms([
							'message' => $message,
							'to' => $cd->phone
						]);
					}
				}

				return $this->render->json([
					'data' => $data,
					'status'=>200,
					'message'=> "{$transType} was made successfully"
				], 200);
				
			}
			return $this->render->json([
				'data' => [],
				'status'=>400,
				'message'=> 'Duplicate transaction'
			], 200);
		}

		return $this->render->json([
			'data' => [],
			'status'=>300,
			'message'=> $this->form_validation->error_array()
		], 200);	
	}

	//updates the balance of the card by calculatign the total sum of transactions as card balance
	//and updating the total withdrawable balance.
	public function update_card_balance($card_id)
	{
		$this->load->model("Mdl_cards");
		$transactions = $this->Mdl_transactions->get_where_custom("card_id", $card_id);

		if ( !empty($transactions) )
		{
			$transList = $transactions->result_array();
			$total = array_sum(array_column($transList, 'amount'));
			$rate = $transList[0]['amount'];
			$withdrawable = $total - $rate;

			$this->Mdl_cards->_update($transList[0]['card_id'], array(
				'card_balance' => $total,
				'card_wbalance'=> $withdrawable
			));
		}
	}
	
	public function update_dr_card_balance($card_id)
	{
		$this->load->model("Mdl_cards");
		//get all transactions where card id matches $card_id
		$transactions = $this->Mdl_transactions->get_where_custom("card_id", $card_id);

		if ( !empty($transactions) )
		{
			$this->Mdl_cards->_update($card_id, array(
				'status' => "pending withdrawal"
			));
		}
	}

	function json_get_card_transactions($card)
	{
		$where = array( 'cards.card_id' => $card);
		$this->db->where($where);
		$this->db->join('cards', 'cards.card_id = transactions.card_id', 'left');
		$result = $this->Mdl_transactions->get();
		return $this->render->json([
						'data' => $result->result(),
						'status'=>200,
						'message'=> 'ok'
					], 200);
	}

	

	function json_get_transaction_history($userId)
	{
		$this->db->join('cards', 'cards.card_id = transactions.card_id', 'left');
		$result = $this->Mdl_transactions->get_where_custom(['transactions.customer_id' => $userId]);
		return $this->render->json([
						'data' => $result->result(),
						'status'=>200,
						'message'=> 'ok'
					], 200);
	}

	public function json_update($id) {

		$this->form_validation->set_rules('full_name', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required');

		if ($this->form_validation->run() == true) {
			$data = $this->get_data_from_post();
			$result = $this->Mdl_transactions->_update($id, $data);
			return $this->render->json(['data' => $data], 200);
		} else {
			return $this->render->json(['errors' => ['invalid' => "An error occured"]], 200);
		}

		return $this->render->json(['errors' => $this->form_validation->error_array()], 200);
	}

	private function get_data_from_post() {
		

		$data['amount']     = $this->input->post('amount');
		$data['trans_type'] = $this->input->post('trans_type');
		$data['user_id']    = $this->input->post('user_id');
		$data['handler_id'] = get_session_data('ssid');
		$data['card_id']    = $this->input->post('card_no');
		
		$data['no_days'] = $this->input->post('no_days');
		

		if ($this->input->post('trans_type') == 'debit') {
			$data['trans_status'] = 'pending';
		}
		return $data;
	}

	public function getUserBalance($id) {
		return $this->render->json(['data' => $this->Mdl_customers->getUserBalance($id)], 200);
	}
	public function getAllUserBalance($id) {
		$Withdrawable = $this->Mdl_customers->getUserBalance($id);
		$original = $this->Mdl_customers->getUserOBalance($id);
		return $this->render->json(['data' => array(
			'original' => $original,
			'withdrawable' => $Withdrawable
		)], 200);
	}

	function json_reset_system()
	{
		$role = get_session_data('role_id');
		if ( !is_admin($role) )
		{
			return $this->render->json([
				'data'    => [],
				'status'  =>400,
				'message' => 'You dont have the right privillege to carry out this task'
			], 200);
		}

		$this->load->model(['Mdl_customers', 'mdl_cards']);
		$this->Mdl_customers->_delete_all();
		$this->mdl_cards->_delete_all();
		$this->Mdl_transactions->_delete_all();
		return $this->render->json([
			'data'    => [],
			'status'  =>200,
			'message' => 'System reset completed successfully'
		], 200);
	}

	public function json_reset_card_transaction()
	{
		$role = get_session_data('role_id');
		if ( !is_admin($role) )
		{
			return $this->render->json([
				'data'    => [],
				'status'  =>400,
				'message' => 'You dont have the right privillege to carry out this task'
			], 200);
		}
		$this->load->model(['Mdl_customers']);
		$card_id = $this->input->post('card_id');
		if ( !is_numeric($card_id))
		{
			return $this->render->json([
						'data'    => [],
						'status'  =>400,
						'message' => 'Invalid card'
					], 200);
		}
		//check if card is active.
		$card_result = $this->mdl_cards->get_where_custom([
			'card_id' => $card_id,
			'status' => 'active'
		]);
		if ( $card_result->num_rows() == 0 )
		{

			return $this->render->json([
						'data'    => [],
						'status'  =>400,
						'message' => 'This card has already been withdrawn or awaiting withdrawal'
					], 200);
		}
		//check if card has a transaction.
		$result = $this->Mdl_transactions->get_where_custom(['card_id' => $card_id, 'trans_type' => 'credit']);
		if ( $result->num_rows() == 0 )
		{
			return $this->render->json([
						'data'    => [],
						'status'  =>400,
						'message' => 'This card doesnt have a transaction'
					], 200);
		}

		$ct_results   = $result->result();
		$admin_profit = $ct_results[0]->amount;
		$customer_id  = $ct_results[0]->customer_id;
		$w_balance    = array_sum(array_column((array) $ct_results,'amount'));

		$customer = $this->Mdl_customers->get_where($customer_id);

		if ( $customer->num_rows() == 0 )
		{
			return $this->render->json([
						'data'    => [],
						'status'  =>400,
						'message' => 'Customer dont exist'
					], 200);
		}

		$customer_bal  = $customer->row()->balance;
		$customer_wbal = $customer->row()->wbalance;

		$uc_bal = $customer_bal - $w_balance;
		$uc_wbal = $customer_wbal;

		if ( $customer->num_rows() == 1 )
		{
			$uc_wbal= $customer_wbal - ($w_balance - $admin_profit);
		}

		$this->db->trans_start();
		$this->Mdl_customers->_update($customer_id, [
			'wbalance' => $uc_wbal,
			'balance' => $uc_bal
		]);

		$this->Mdl_transactions->_delete_where([
			'card_id' => $card_id
		]);

		$this->mdl_cards->_update($card_id, ['card_balance' => 0, 'card_wbalance' => 0]);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		     return $this->render->json([
						'data'    => [],
						'status'  =>400,
						'message' => 'Card transactions reset was not successful'
					], 200);
		}
		
		return $this->render->json([
						'data'    => [],
						'status'  =>200,
						'message' => 'Card transactions was reset successfully'
					], 200);
	}

	public function json_reset_all_card_transactions()
	{
		$role = get_session_data('role_id');
		if ( !is_admin($role) )
		{
			return $this->render->json([
				'data'    => [],
				'status'  =>400,
				'message' => 'You dont have the right privillege to carry out this task'
			], 200);
		}
		
		$this->db->trans_start();
		$this->Mdl_customers->_update_all([
			'wbalance' =>0,
			'balance' => 0
		]);

		$this->Mdl_transactions->_delete_all();
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
		     return $this->render->json([
						'data'    => [],
						'status'  =>400,
						'message' => 'All card transactions reset was not successful'
					], 200);
		}
		
		return $this->render->json([
						'data'    => [],
						'status'  =>200,
						'message' => 'All card transactions was reset successfully'
					], 200);
	}

}

/* End of file Transactions.php */
/* Location: ./application/controllers/Transactions.php */
