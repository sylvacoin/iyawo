<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('render');
		$this->load->library('debugger');
		$this->load->model(['Mdl_transactions']);
		//is_user_logged();
	}

	public function index() {
		is_admin_logged();
		$this->render->view('dashboard/index', 'My Dashboard', null, 'backend');
	}
}
