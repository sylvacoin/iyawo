<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->model('Mdl_menus');
		
	}
	public function index()
	{
	    is_user_logged();
		redirect('menu/customize');
	}

	public function customize() {
	    is_user_logged();
		$this->render->view('menu/index', 'Menu', null, 'backend');
	}

	public function get_json() {
		return $this->render->json(['data' => $this->Mdl_menus->get(false)], 200);
	}

	public function json_get_all() {
		return $this->render->json(['data' => $this->Mdl_menus->get()], 200);
	}

	public function order() {
		$menus = json_decode(file_get_contents('php://input'));
		
		$menu_list = (array) json_decode($menus);
		$new_menu_update = [];
		//$this->debugger->debug($menu_list);
		foreach ($menu_list as $key => $menu) {
			$new_menu_update[] = array(
				'menu_id' => $menu->menu_id,
				'parent_id' => 0,
				'menu_order' => $key + 1,
			);
			if (isset($menu->sub_menus) && count($menu->sub_menus) > 0) {
				$sub_menu_list = (array) $menu->sub_menus;
				foreach ($sub_menu_list as $k => $sub_menus) {
					$new_menu_update[] = array(
						'menu_id' => $sub_menus->menu_id,
						'parent_id' => $menu->menu_id,
						'menu_order' => $k + 1,
					);
				}
			}
		}
		$res = $this->Mdl_menus->_update_batch($new_menu_update, 'menu_id');
		if ($res > 0) {
			return $this->render->json(['data' => 'success'], 200);
		}
		return $this->render->json(['errors' => ['No update was carried out']], 200);
	}

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */
