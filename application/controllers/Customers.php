<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['mdl_customers', 'mdl_users']);
	}

	public function index()
	{
	    is_user_logged();
		$this->render->view('customers/index.php', 'All Customers', [], 'backend');
	}

	public function handler_customers($id = null )
	{
	    is_user_logged();
		$name = "";
		if ( is_numeric($id) )
		{
			$user = $this->mdl_users->get_where($id);
			if ( !empty($user) )
			{
				$name = $user->row()->firstname." ".$user->row()->lastname;
			}
			$this->render->view('customers/view_handler_customers.php', $name.'\'s Customers', [
				'name' => $name
			], 'backend');
			return; 
		}

		$this->render->view('customers/handler_customers.php', 'My Customers', [], 'backend');
	}

	public function profile($id)
	{
	    is_user_logged();
	    //is_customer_logged();
	    //$this->db->where('is_flagged', 0);
		$data = $this->mdl_customers->get_where($id)->row();
		$this->render->view('customers/profile.php', "{$data->full_name} Profile ({$data->gender})", ['phone' => $data->phone], 'backend');
	}

	public function flagged()
	{
	    //is_customer_logged();
	    is_user_logged();
		$this->render->view('customers/flagged.php', 'Customers', [], 'backend');
	}



	//API
	public function json_get_customers()
	{
		$this->db->where('is_flagged', 0);
		$data = $this->mdl_customers->get('customer_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_reset_customers()
	{
		$role = get_session_data('role_id');
		if ( !is_admin($role) )
		{
			return $this->render->json([
				'data'    => [],
				'status'  =>400,
				'message' => 'You dont have the right privillege to carry out this task'
			], 200);
		}
		
		$data = $this->mdl_customers->_delete_all(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'All customers was erased successfully'
		], 200);
	}

	public function json_get_handler_customers($handler = null)
	{
		if ( $handler )
		{
			$handler_id = $handler;
		}else{
			$handler_id = get_session_data('ssid');
		}
		
		$this->db->where('handler_id', $handler_id);
		$this->db->where('is_flagged', 0);
		$data = $this->mdl_customers->get('customer_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_get_flagged_customers()
	{
		$this->db->where('is_flagged', 1);
		$data = $this->mdl_customers->get('customer_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_post_customer()
	{
		$this->form_validation->set_rules('full_name', 'Full Name', 'required');

		if ( $this->form_validation->run())
		{
			$data = $this->get_data_from_post();
			$result = $this->mdl_customers->_insert($data);
			$data['customer_id'] = $this->db->insert_id();
			$data['balance'] = 0;
			$data['wbalance'] = 0;

//TODO: send an sms notification to client here

			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'New customer was created successfully'
			], 201);
		}

		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_update_customer($id)
	{

		$this->form_validation->set_rules('full_name', 'Full Name', 'required');

		if ( $this->form_validation->run() == true)
		{
			$data = $this->get_data_from_post();
			$result = $this->mdl_customers->_update($id, $data);
			
			if ( $result )
			{
				return $this->render->json([
					'data' => $data,
					'status' => 200,
					'message' => 'Customer was updated successfully'
				], 200);
			}
		}else{
			return $this->render->json([
				'data' => [],
				'status' => 400,
				'message' =>  'An error occured customer not updated'
			], 200);
		}

		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}


	private function get_data_from_post()
	{		
		$data['full_name']   = $this->input->post('full_name');
		$data['customer_no'] = $this->input->post('customer_no');
		$data['phone'] = $this->input->post('phone_no');
		$data['gender'] = $this->input->post('gender');
		$data['handler_id']  = get_session_data('ssid');
		$data['has_alert']  = $this->input->post('alert_status');

		return $data;
	}

	public function json_unflag_customer($id)
	{
		$data = array( 
			'is_flagged'=>0
		);
		$result = $this->mdl_customers->_update($id, $data);
		if ( $result )
		{
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'Customer was unflagged successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_flag_customer($id)
	{
		$data = array( 
			'is_flagged'=>1
		);
		$result = $this->mdl_customers->_update($id, $data);
		if ( $result )
		{
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'Customer was flagged successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_delete_customer($id)
	{
		$result = $this->mdl_customers->_delete($id);
		if ( $result )
		{
			return $this->render->json([
				'data' => [],
				'status' => 200,
				'message' => 'Customer was deleted successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_count_customers()
	{
		$role = get_session_data('role_id');

		if ( is_admin($role) )
		{
			$data = $this->mdl_customers->get();
		}else{
			$user = get_session_data('ssid');
			$data = $this->mdl_customers->get_where_custom('handler_id', $user);
		}		
		return $this->render->json(['data' => $data->num_rows()], 200);
	}

	public function json_count_handler_customers()
	{
		$handler_id = get_session_data('ssid');
		$data = $this->mdl_customers->get_where_custom(['handler_id' => $handler_id]);
					
		return $this->render->json(['data' => $data->num_rows()], 200);
	}
}

/* End of file Customers.php */
/* Location: ./application/controllers/Customers.php */