<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_groups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_user_groups');
	}

	public function index()
	{
	    is_user_logged();
		$data = $this->mdl_user_groups->get();
		return $this->render->view('user_groups/index', 'User groups / privilege', ['data' => $data->result()], 'backend');
	}

	public function assign_privileges()
	{
	    is_user_logged();
		$data = $this->mdl_user_groups->get();
		return $this->render->view('user_groups/assign_privileges', 'User groups / privilege', ['data' => $data->result()], 'backend');
	}

	//API's all apis starts with json

	function json_get_user_groups() {
		if ( get_session_data('role_id') != 1)
		{
			$this->db->where('user_group_id != 1');
		}
		$user_groups = $this->mdl_user_groups->get()->result();
		return $this->render->json(
			[
				'data' => $user_groups,
				'status' => 200, 
				'message'=>'ok'
			], 200);
	}

	function json_get_user_group_menus($id) {
		$menus = [];
		$rows = $this->mdl_user_groups->get_where($id);
		if ($rows->num_rows() > 0) {
			$row = $rows->row()->user_group_menus;
			$menus['user_group'] = $rows->row()->user_group;
			$row_array = explode(',', $row);
			foreach ($row_array as $k => $v) {
				$menus['selected'][$v] = true;
			}
		}
		return $this->render->json([
			'data' => $menus,
			'status' => 200,
			'message' => 'ok'
		], 200);
	}

	function json_post_user_group() {
		$this->form_validation->set_rules('user_group', 'User group', 'required');

		if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();
			$result = $this->mdl_user_groups->_insert($data);
			if ($result) {
				$data['user_group_id'] = $this->db->insert_id();
				return $this->render->json(
					[
						'data' 		=> $data,
						'message' 	=> 'User group was added succesfully',
						'status'	=> 200
					], 201);
			}

			return $this->render->json(
				[
					'data'	  => [],
					'status'  => 400,
					'message' => 'An error occured creating user group'
				], 200);
		}

		return $this->render->json([
			'data'	  => [],
			'status'  => 300,
			'message' => $this->form_validation->error_array()
		], 200);
	}

	public function json_post_user_group_menus($id) {
		$data = (array) json_decode(file_get_contents('php://input'));
		$selected = array_filter($data, function ($v) {
			return $v == 1;
		});
		$user_group_menus = implode(",", array_keys($selected));
		if ($this->mdl_user_groups->_update($id, ['user_group_menus' => $user_group_menus])) {
			return $this->render->json([
				'data' => [],
				'status' => 200,
				'message' => 'ok'
			], 200);
		}

		return $this->render->json([
				'data' => 'An error occured while updating menu',
				'status' => 400,
				'message' => 'ok'
			], 200);
	}

	function json_put_user_group($id) {
		$this->form_validation->set_rules('user_group', 'User group', 'required');
		if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();
			$result = $this->mdl_user_groups->_update($id, $data);
			if ($result) {
				return $this->render->json([
					'data'    => $data,
					'message' => 'User group was updated succesfully',
					'status'  => 200
				], 200);
			}

			return $this->render->json(
				[
					'data'	  => [],
					'status'  => 400,
					'message' => 'An error occured updating user group'
				], 200);
		}

		return $this->render->json([
			'data'	  => [],
			'status'  => 300,
			'message' => $this->form_validation->error_array()
		], 200);
	}

	function json_delete_user_group($id) {
		if ($this->mdl_user_groups->_delete($id)) {
			return $this->render->json([
				'message' => 'User group was deleted succesfully',
				'status' => 200,
				'data' => [],
			], 200);
		}
		return $this->render->json(['errors' => ['Can not be deleted. User group dont exist']], 200);
	}

	//Helper Methods
	private function get_data_from_post() {
		$data['user_group'] = $this->input->post('user_group');
		$data['account_type'] = $this->input->post('access_type');
		$data['is_default'] = $this->input->post('is_default') == 'true' ? true : false;
		return $data;
	}


}

/* End of file User_groups.php */
/* Location: ./application/controllers/User_groups.php */