<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('mailer', array(
			'to' => 'hclinton007@gmail.com',
            'template' => 'new_admin_account',
            'fields' => array(
            	'@@FIELD_1@@' => 'Hillary',
            	'@@FIELD_2@@' => 'dogpassword'
            )
		));
	}

	public function mail()
	{
		$this->mailer->send_mail('hclinton007@gmail.com', 'Test Message', '');
	}

}

/* End of file Test.php */
/* Location: ./application/controllers/Test.php */