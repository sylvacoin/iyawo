<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	    $this->load->model('Mdl_sms');
	    $this->load->helper('email');
	}

	function index() {
        //is_user_logged();
        $mail_templates = $this->Mdl_sms->get();
        $this->render->view('sms/index', 'SMS Template List', ['data' => $mail_templates], 'backend');
    }
    function modify($id) {
        $row = $this->Mdl_sms->get_where($id)->row();
        if ($row) {
            $this->render->view('sms/modify', 'SMS Settings', ['data' => $row], 'backend');
            return;
        }
        $this->session->set_flashdata('error', 'invalid mail data');
        redirect('/sms');
    }
    function submit($id) {
        $this->form_validation->set_rules('subject', 'SMS subject', 'required');
        $this->form_validation->set_rules('body', 'SMS body', 'required');
        if ($this->form_validation->run()) {
            $this->submit_data();
        } else {
            $this->session->set_flashdata('error', 'An error occured please try again later');
        }
        redirect('sms/modify/' . $id);
    }
    function submit_data() {
        //get data from post
        $data = $this->get_data_from_post();
        $id = $this->uri->segment(3);
        if (is_numeric($id)) {
            $this->Mdl_sms->_update($id, $data);
        } else {
            $this->Mdl_sms->_insert($data);
        }
    }
    private function get_data_from_post() {
        return array('body' => $this->input->post('body'));
    }
}

/* End of file Sms.php */
/* Location: ./application/controllers/Sms.php */