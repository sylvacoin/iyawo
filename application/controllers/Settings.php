<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Mdl_settings']);
	}

	public function index()
	{
		is_user_logged();
		$this->render->view('settings/index.php', 'Site Configuration', [], 'backend');
	}

	public function test()
	{
		$this->load->library('Sms');
		$this->sms->send_sms(['to'=>'08137919696', 'message'=>'CARDNO: %(CardNo)\r\nCOUNT: (%(TransCount))\r\n BAL: %(CardBal)\r\n DD: %(DepositDate)']);
	}

	public function test2()
	{
		$res = json_decode('{"code":"1002","error":true,"comment":"No valid phone number found. 1 (2348166897526) phone number(s) suppressed to prevent flooding"}');
		$code = 'code';

		$this->debugger->debug($res->$code);
	}

	public function send_sms($data)
	{
		extract($data);
		$token = $this->get_setting('sms_api_token');
		$baseurl = $this->get_setting('sms_api');
		$senderid = $this->get_setting('sms_sender_id');
		$status = $this->get_setting('sms_response_key');
		$success = $this->get_setting('sms_response_success');

		$baseurl = str_replace('%(TOKEN)', $token, $baseurl);
		$baseurl = str_replace('%(MESSAGE)', urlencode($message), $baseurl);
		$baseurl = str_replace('%(SENDER)', urlencode($senderid), $baseurl);
		$baseurl = str_replace('%(TO)', urlencode($to), $baseurl);

		//$this->debugger->debug($success);

		$ch = curl_init(); 

		curl_setopt($ch, CURLOPT_URL,$baseurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

		$response = curl_exec($ch);

		curl_close($ch);
		$res = json_decode($response);

		if ( $res->$status == $success)
		{
			return true;
		} // response code

		return false;
	}

	public function get_balance()
	{

	}

	public function get_setting( $key )
	{
		$result = $this->Mdl_settings->get_where_custom('skey', $key);
		if ( $result->num_rows() > 0 )
		{
			return $result->row()->svalue;
		}
		return null;
	}

	public function set_setting( $key, $value )
	{
		$response = $this->Mdl_settings->get_where_custom('skey', $key);
		if ( $response->num_rows() > 0 )
		{
			$id = $response->row()->setting_id;
			return $this->Mdl_settings->_update($id, ['svalue' => $value] );
		}

		return $this->Mdl_settings->_insert(['skey' => $key, 'svalue' => $value] );
	}

	public function get_api_result()
	{
		
	}

}

/* End of file Settings.php */
/* Location: ./application/controllers/Settings.php */