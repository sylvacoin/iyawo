<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cards extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_cards');
	}

	public function index()
	{
	    is_user_logged();
		$this->render->view('cards/index.php', 'Cards', [], 'backend');
	}

	public function details($id)
	{
	    is_user_logged();
	    //$this->db->where('is_flagged', 0);
		$data = $this->mdl_cards->get_where($id)->row();
		$this->render->view('cards/details.php', "{$data->card_no}", [], 'backend');
	}

	public function flagged()
	{
	    is_user_logged();
		$this->render->view('cards/flagged.php', 'Cards', [], 'backend');
	}

	function test()
	{
		return $this->render->json([
			'data' => $this->mdl_cards->get_total_balance(),
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	//API
	
	public function json_get_cards($id)
	{
	    $result = $this->mdl_cards->get_where_custom_join([
			'cards.customer_id' => $id
		]);
		$data['count'] = $result->num_rows(); 
		$data['cards'] = $result->result();
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_get_handler_cards()
	{
		$handler_id = get_session_data('ssid');
		
		$this->db->where('handler_id', $handler_id);
		$this->db->where('is_flagged', 0);

		$data = $this->mdl_cards->get('card_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_get_flagged_cards()
	{
		$this->db->where('is_flagged', 1);
		$data = $this->mdl_cards->get('card_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_post_card()
	{
		$this->form_validation->set_rules('card_no', 'Card number', 'required|is_unique[cards.card_no]');
		if ( $this->form_validation->run())
		{
			$data = $this->get_data_from_post();
			$result = $this->mdl_cards->_insert($data);
			$data['card_id'] = $this->db->insert_id();
			$data['updated_at'] = 'now';
			$data['status'] = 'active';
			$data['card_wbalance'] = 0;
			$data['total'] = 0;

			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'New card was created successfully'
			], 201);
		}

		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_update_card($id)
	{

		$this->form_validation->set_rules('card_no', 'Card number', 'required');

		if ( $this->form_validation->run() == true)
		{
			$data = $this->get_data_from_post();
			$result = $this->mdl_cards->_update($id, $data);
			$data['updated_at'] = 'now';
			
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'Card was updated successfully'
			], 200);
		}else{
			return $this->render->json([
				'data' => [],
				'status' => 400,
				'message' =>  'An error occured card not updated'
			], 200);
		}

		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}


	private function get_data_from_post()
	{
		$data['card_no']      = $this->input->post('card_no');
		$data['card_start']   = $this->input->post('card_start');
		$data['start_amount'] = $this->input->post('start_amount');
		$data['customer_id']  = $this->input->post('user_id');
		return $data;
	}

	public function json_unflag_card($id)
	{
		$data = array( 
			'is_flagged'=>0
		);
		$result = $this->mdl_cards->_update($id, $data);
		if ( $result )
		{
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'Card was unflagged successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_flag_card($id)
	{
		$data = array( 
			'is_flagged'=>1
		);
		$result = $this->mdl_cards->_update($id, $data);
		if ( $result )
		{
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'Card was flagged successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_delete_card($id)
	{
		$result = $this->mdl_cards->_delete($id);
		if ( $result )
		{
			return $this->render->json([
				'data' => [],
				'status' => 200,
				'message' => 'Card was deleted successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}
}

/* End of file Cards.php */
/* Location: ./application/controllers/Cards.php */