<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_users');
	}

	public function index()
	{
	    is_admin_logged();
		$this->render->view('users/index.php', 'All Staffs', [], 'backend');
	}

	public function flagged()
	{
	    is_admin_logged();
		$this->render->view('users/flagged.php', 'Flagged Users', [], 'backend');
	}

	public function json_is_admin()
	{
		$role_id = get_session_data('role_id');
		$data['is_admin'] = is_admin($role_id);
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	//API
	
	public function json_get_users()
	{
		$this->db->where('account_type', 'admin');
		$this->db->where('is_flagged', 0);
		$data = $this->mdl_users->get('user_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_count_users()
	{
		$this->db->where('account_type', 'admin');
		$this->db->where('is_flagged', 0);
		
		$data = $this->mdl_users->get();
		return $this->render->json(['data' => $data->num_rows()], 200);
	}

	public function json_get_flagged_users()
	{
		
		$this->db->where('account_type', 'admin');
		$this->db->where('is_flagged', 1);
		$data = $this->mdl_users->get('user_id')->result(); 
		return $this->render->json([
			'data' => $data,
			'status'=>200,
			'message'=> 'Ok'
		], 200);
	}

	public function json_post_user()
	{
		$this->form_validation->set_rules('firstname', 'First Name', 'required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ( $this->form_validation->run())
		{
			$data = $this->get_data_from_post();
			$result = $this->mdl_users->_insert($data);
			$data['user_id'] = $this->db->insert_id();
			unset($data['password']);
			$send_mail = $this->input->post('send_mail');

			if ( $send_mail )
			{
				$this->load->library('MailerLite', array(
                    'to' => $data['email'],
                    'fields' => array(
                    	'@@FIELD_1@@' => $data['firstname'].' '.$data['lastname'],
                    	'@@FIELD_2@@' => $this->input->post('password'),
                    ),
                    'template' => 'new_admin_account'
                ));
                $this->mailerlite->send_mail();
			}
			
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'New user was created successfully'
			], 201);
		}

		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_update_user($id)
	{

		$this->form_validation->set_rules('firstname', 'First Name', 'required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required');

		if ( $this->form_validation->run() == true)
		{
			$data = $this->get_data_from_post();
			$result = $this->mdl_users->_update($id, $data);
			unset($data['password']);
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'User was updated successfully'
			], 200);
		}else{
			return $this->render->json([
				'data' => [],
				'status' => 400,
				'message' =>  'An error occured user not updated'
			], 200);
		}

		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}


	private function get_data_from_post()
	{
		$data['firstname'] = $this->input->post('firstname');
		$data['lastname'] = $this->input->post('lastname');
		$data['email'] = $this->input->post('email');
		
		$pswd = $this->input->post('password');
		if (  !empty($pswd) )
		{
			$data['password'] = md5($pswd. SALT);
		}
		if ( $this->input->post('role_id') != null )
		{
		    $data['user_group_id'] = $this->input->post('role_id');
		}
		
		return $data;
	}

	public function json_unflag_user($id)
	{
		$data = array( 
			'is_flagged'=>0
		);
		$result = $this->mdl_users->_update($id, $data);
		if ( $result )
		{
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'User was unflagged successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_flag_user($id)
	{
		$data = array( 
			'is_flagged'=>1
		);
		$result = $this->mdl_users->_update($id, $data);
		if ( $result )
		{
			return $this->render->json([
				'data' => $data,
				'status' => 200,
				'message' => 'User was flagged successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

	public function json_delete_user($id)
	{
		$result = $this->mdl_users->_delete($id);
		if ( $result )
		{
			return $this->render->json([
				'data' => [],
				'status' => 200,
				'message' => 'User was deleted successfully'
			], 200);
		}
		
		return $this->render->json([
			'data' => [],
			'status' => 300,
			'message' =>  $this->form_validation->error_array()
		], 200);
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */