<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_auth');
	}

	public function index()
	{
		
	}



	//PAGES

	public function login() {
		$this->render->view('auth/login', 'Login', null, 'frontend');
	}

	public function password_reset() {
		$this->render->view('auth/password_reset', 'Reset password', null, 'frontend');
	}
	
	public function change_password() {
		$this->render->view('auth/change_password', 'Reset password', null, 'backend');
	}

	public function reset($token = null) {
		if ( empty($token) )
		{
			redirect('login');
		}

		$result = $this->mdl_auth->get_where_custom([
			'reset_token' => $token
		]);

		if ( $result->num_rows() == 0 )
		{
			redirect('login');
		}

		$this->render->view('auth/password_reset_change', 'Reset password', ['token' => $token], 'frontend');
	}


	//API

	public function json_do_login() {

		$this->form_validation->set_rules('email', 'Email Address', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) //login logic for login page
		{
			$result = $this->mdl_auth->get_where_custom([
				'email' => $this->input->post('email'),
				'password' => hash_password(trim($this->input->post('password'))),
			]);
			

			if ($result->num_rows() > 0) {
				set_session_data('ssid', $result->row()->user_id);
				set_session_data('name', "{$result->row()->firstname} {$result->row()->lastname}");
				set_session_data('role_id', $result->row()->user_group_id);
				set_session_data('email', $result->row()->email );
				return $this->render->json([
					'data'=>[],
					'message' => 'Login was successful',
					'status' => 200,
					'url' => base_url().'dashboard'
				], 200);
			} else {
				return $this->render->json([
					'data'=>['r'=>hash_password(trim($this->input->post('password'))), 'l' =>hash_password('codex007')],
					'status' => 400,
					'message' => 'invalid username or password'
				], 200);
			}
		}

		return $this->render->json(['errors' => $this->form_validation->error_array()], 400);
	}

	public function json_reset_password() {
		$this->form_validation->set_rules('email', 'Email Address', 'required');

		if ($this->form_validation->run()) //login logic for login page
		{
			$result = $this->mdl_auth->get_where_custom([
				'email' => $this->input->post('email'),
			]);

			if ($result->num_rows() > 0) {
				//do the reset here
				$user = $result->row();
				$token = md5(random_string('alnum', 32));
				$reset_link = base_url('reset/').$token;

				$this->load->library('MailerLite', array(
					'to' => $this->input->post('email'),
					'fields' => array(
							'@@FIELD_1@@' => $user->firstname." ".$user->lastname,
							'@@FIELD_2@@' => $reset_link
						),
					'template' => 'password_reset'
				));

				$this->mdl_auth->_update($user->user_id, ['reset_token' => $token]);
				if ($this->mailerlite->send_mail()) {
					return $this->render->json(['message' => 'Please check your email for reset link.'], 200);
				} else {
					return $this->render->json(['message' => 'An error occured please try again. or contact administrator'], 400);
				}

			}

		}

		return $this->render->json(['error' => 'This email address does not exist'], 400);
	}


	public function json_reset_password_complete() {
	    
		$this->form_validation->set_rules('token', 'Token', 'required');

		if ($this->form_validation->run()) //login logic for login page
		{
			$result = $this->mdl_auth->get_where_custom([
				'reset_token' => $this->input->post('token'),
			]);

			if ($result->num_rows() > 0) {
				//do the reset here
				$user = $result->row();
				$new_password = hash_password( $this->input->post('password')  );
				//$this->debugger->debug($new_password);

				$this->load->library('MailerLite', array(
					'to' => $user->email,
					'fields' => array(
							'@@FIELD_1@@' => $user->firstname." ".$user->lastname,
						),
					'template' => 'password_changed'
				));

				$this->mdl_auth->_update($user->user_id, ['reset_token' => NULL, 'password' => $new_password]);
				if ($this->mailerlite->send_mail()) {
					return $this->render->json(['message' => 'Password reset was successful. Redirecting to login...'], 200);
				} else {
					return $this->render->json(['message' => 'An error occured please try again. or contact administrator'], 400);
				}

			}

		}

		return $this->render->json(['error' => 'Invalid token'], 400);
	}

	public function json_change_password() {
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');

		if ($this->form_validation->run()) //login logic for login page
		{
			//do the reset here
			$user_id = get_session_data('ssid');

			$user = $this->mdl_auth->get_where($user_id);
            
			if ($user->num_rows() > 0) {
				$this->load->library('MailerLite', array(
					'to' => $user->row()->email,
					'fields' => array(
							'@@FIELD_1@@' => $user->row()->firstname." ".$user->row()->lastname,
						),
					'template' => 'password_changed'
				));

				$this->mdl_auth->_update($user_id, ['password' => md5($this->input->post('password') . SALT)]);
				//$this->debugger->debug($user->num_rows());
				if ($this->mailerlite->send_mail()) {
					return $this->render->json(['message' => 'Password was changed succesfully'], 200);
				}
				return $this->render->json(['message' => 'Password was changed succesfully'], 200);
			}
			return $this->render->json(['error' => 'Invalid user account'], 401);
		}
		return $this->render->json(['errors' => $this->form_validation->error_array()], 400);
	}

	public function logout() {
	    is_user_logged();
		session_destroy();
		redirect('login');
	}

	private function set_session($key, $value) {
		$this->session->set_userdata(
			[
				'sess' => [
					SITENAME => [$key => $value],
				],
			]
		);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */