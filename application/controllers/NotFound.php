<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NotFound extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->render->view('notFound/index', 'Not Found', [], 'backend');
	}

}

/* End of file NotFound.php */
/* Location: ./application/controllers/NotFound.php */