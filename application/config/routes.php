<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['404_override'] = '';
$route['default_controller'] = 'dashboard';
$route['translate_uri_dashes'] = FALSE;

//Pages
$route['user-groups'] = 'user_groups';
$route['customers/my-customers'] = 'customers/handler_customers';
$route['transactions/view/(:any)'] = 'transactions/view/$1';
$route['user-groups/assign-privileges/(:num)'] = 'user_groups/assign_privileges/$1';
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';
$route['reset/(:any)'] = 'auth/reset/$1';
$route['forgot-password'] = 'auth/password_reset';
$route['change-password'] = 'auth/change_password';
$route['customers/handler/(:num)/view-customer'] = 'customers/handler_customers/$1';
$route['my-customers'] = 'customers/handler_customers';

//API
$route['user-groups/get-user-groups'] = 'user_groups/json_get_user_groups';
$route['user-groups/post-user-group'] = 'user_groups/json_post_user_group';
$route['user-groups/delete-user-group/(:num)'] = 'user_groups/json_delete_user_group/$1';
$route['user-groups/put-user-group/(:num)'] = 'user_groups/json_put_user_group/$1';
$route['users/get-users'] = 'users/json_get_users';
$route['users/get-flagged-users'] = 'users/json_get_flagged_users';
$route['users/create-user'] = 'users/json_post_user';
$route['users/update-user/(:num)'] = 'users/json_update_user/$1';
$route['users/unflag-user/(:num)'] = 'users/json_unflag_user/$1';
$route['users/flag-user/(:num)'] = 'users/json_flag_user/$1';
$route['users/delete-user/(:num)'] = 'users/json_delete_user/$1';
$route['users/is-admin'] = 'users/json_is_admin';

$route['customers/get-customers'] = 'customers/json_get_customers';
$route['customers/get-handler-customers'] = 'customers/json_get_handler_customers';
$route['customers/get-handler-customers/(:num)'] = 'customers/json_get_handler_customers/$1';
$route['customers/get-flagged-customers'] = 'customers/json_get_flagged_customers';
$route['customers/create-customer'] = 'customers/json_post_customer';
$route['customers/update-customer/(:num)'] = 'customers/json_update_customer/$1';
$route['customers/unflag-customer/(:num)'] = 'customers/json_unflag_customer/$1';
$route['customers/flag-customer/(:num)'] = 'customers/json_flag_customer/$1';
$route['customers/delete-customer/(:num)'] = 'customers/json_delete_customer/$1';
$route['customers/reset-customers'] = 'customers/json_reset_customers';



$route['cards/get-cards/(:num)'] = 'cards/json_get_cards/$1';
$route['cards/get-flagged-cards'] = 'cards/json_get_flagged_cards';
$route['cards/create-card'] = 'cards/json_post_card';
$route['cards/update-card/(:num)'] = 'cards/json_update_card/$1';
$route['cards/unflag-card/(:num)'] = 'cards/json_unflag_card/$1';
$route['cards/flag-card/(:num)'] = 'cards/json_flag_card/$1';
$route['cards/delete-card/(:num)'] = 'cards/json_delete_card/$1';

$route['transactions/create-transaction'] = 'transactions/json_post_transaction';
$route['transactions/get-card-transactions/(:num)'] = 'transactions/json_get_card_transactions/$1';
$route['transactions/get-transaction-history/(:num)'] = 'transactions/json_get_transaction_history/$1';
$route['transactions/get-all-transactions'] = 'transactions/json_get_all_transactions';
$route['transactions/get-handler-transactions'] = 'transactions/json_get_handler_transactions';
$route['my-transactions'] = 'transactions/handler_transactions';
$route['transactions/get-transactions/(:any)'] = 'transactions/json_get_transactions/$1';
$route['transactions/reset-card-transaction'] = 'transactions/json_reset_card_transaction';
$route['transactions/reset-all-card-transactions'] = 'transactions/json_reset_all_card_transactions';
$route['reset'] = 'transactions/json_reset_system';



$route['menu/get-all'] = 'menu/json_get_all';
$route['user-groups/get-user-group-menus/(:num)'] = 'user_groups/json_get_user_group_menus/$1';
$route['user-groups/create-user-group-menus/(:num)'] = 'user_groups/json_post_user_group_menus/$1';	


$route['api/reset-password'] = 'auth/json_reset_password';
$route['api/change-password'] = 'auth/json_change_password';
$route['api/reset-password-complete'] = 'auth/json_reset_password_complete';
$route['api/count-customers'] = 'customers/json_count_customers';
$route['api/count-users'] = 'users/json_count_users';


$route['auth/do-login'] = 'auth/json_do_login';
