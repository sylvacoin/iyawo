<?php

//site wide constants;
define('DIV_ERROR', '<div class="alert alert-danger">');
define('DIV_SUCCESS', '<div class="alert alert-success">');
define('DIV_CLOSE', '</div>');

if ( !function_exists('show_message') )
{
    function show_message($success, $error) {
        if (isset($success)) {
            echo DIV_SUCCESS . $success . DIV_CLOSE;
        }

        if (isset($error)) {
            echo DIV_ERROR . $error . DIV_CLOSE;
        }

        echo validation_errors(DIV_ERROR, DIV_CLOSE);
    }


}
