<?php

function get_session_data($key) {
	$ci = &get_instance();
	if (array_key_exists(SESSNAME, $_SESSION) && array_key_exists($key, $ci->session->userdata(SESSNAME))) {
		return $_SESSION[SESSNAME][$key];
	}

	return [];
}

function set_session_data($key, $value) {
	$_SESSION[SESSNAME][$key] = $value;
}

function is_user_logged() {

	$uid = get_session_data('ssid');
	$role = get_session_data('role_id');
	set_session_data('previous_url', current_url());
	if (!is_numeric($uid) && !hasCookie() ) {
		redirect('/login');
	}
}

function is_admin_logged() {
	$uid = get_session_data('ssid');
	$role = get_session_data('role_id');
	set_session_data('previous_url', current_url());

	if (!is_numeric($uid) && !hasCookie()) {
		redirect('login');
	}

	if (hasCookie()){
		$uid = get_session_data('ssid');
		$role = get_session_data('role_id');

		if (!is_admin($role)){
			redirect('login');
		}
	}
	//print_r($_SESSION); die();
	
}

function is_admin( $role )
{
	return $role == 1 || $role == 6;
}

function clearAuthCookie() {
    if (isset($_COOKIE["member_login"])) {
        //setcookie("member_login", "");
        set_cookie("member_login", "");
    }
    if (isset($_COOKIE["random_password"])) {
        // setcookie("random_password", "");
        set_cookie("random_password", "");
    }
    if (isset($_COOKIE["random_selector"])) {
        //setcookie("random_selector", "");
        set_cookie("random_selector", "");
    }
}

function hasCookie()
{
	$ci = &get_instance();
	$ci->load->model(['mdl_token_auth','mdl_users']);
	$member_login = get_cookie("member_login");
	$random_password = get_cookie("random_password");
	$random_selector = get_cookie("random_selector");

	// Get Current date, time
	$current_time = time();
	$current_date = date("Y-m-d H:i:s", $current_time);

	if (! empty($member_login) && ! empty($random_password) && ! empty($random_selector)) {
	    // Initiate auth token verification diirective to false
	    $isPasswordVerified = false;
	    $isSelectorVerified = false;
	    $isExpiryDateVerified = false;
	    
	    // Get token for username
	    $token = $ci->mdl_token_auth->get_where_custom([
	    	'username'=>$member_login
	    ]);
	    
	    // Validate random password cookie with database
	    if ($token && password_verify($random_password, $token->row()->password_hash )) {
	        $isPasswordVerified = true;
	    }
	    
	    // Validate random selector cookie with database
	    if ($token && password_verify($random_selector, $token->row()->selector_hash)){
	    // Redirect if all cookie based validation retuens t->selector_hash)) {
	        $isSelectorVerified = true;
	    }
	    
	    // check cookie expiration by date
	    if($token && $token->row()->expiry_date >= $current_date) {
	        $isExpiryDateVerified = true;
	    }
	    // Else, mark the token as expired and clear cookies

	    if (!empty($token) && $isPasswordVerified && $isSelectorVerified && $isExpiryDateVerified) {
	        
	        $id = explode('YY', $member_login)[0];
	        $user = $ci->mdl_users->get_where($id);
            
            if ( $user->num_rows() > 0 ){
                //load data to session
    	        set_session_data('ssid', $user->row()->ssid);
    			set_session_data('name', $user->row()->firstname." ".$user->row()->lastname);
    			set_session_data('role_id', $user->row()->user_group_id);
    			set_session_data('email', $user->row()->email);
    
    	        return true;
            }
	        
	    } else {
	        if(!empty($member_login)) {
	            $ci->mdl_token_auth->markAsExpired($member_login);
	        }
	        // clear cookies
	       clearAuthCookie();
	    }
	}

	return false;
}

function faux_session()
{
	set_session_data('ssid', 4);
}