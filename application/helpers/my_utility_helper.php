<?php

if ( !function_exists('get_gravatar') ){

    function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
        if ( empty($email) ){
            return base_url().(DEFAULT_AVATAR ? DEFAULT_AVATAR : '');
        }

        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

}

if ( !function_exists('set_active') ){
    function set_active($link, $seg = 0) {
        $uri = uri_string();
        $ci = &get_instance();

        if ( !is_array($link) && $link == $ci->uri->segment(1).'/'.$ci->uri->segment(2)) {
            return 'active ';
        }elseif(is_array($link) && (in_array($ci->uri->segment(1), $link) || in_array($ci->uri->segment(2), $link) ) ){
            return 'active ';
        }
        return '';
    }
}

if ( !function_exists('get_system_overall_balance') ){
    function get_system_overall_balance() {
        $role = get_session_data('role_id');
        if (is_admin($role))
        {
            $ci = &get_instance();

            $ci->load->model(['Mdl_customers','Mdl_transactions']);
            $balance = $ci->Mdl_customers->get_total_balance();
            $profit = $ci->Mdl_transactions->get_total_profit();
            $result = $balance + $profit;
            
            return "OVR BAL: ".DEFAULT_CURRENCY . number_format($result, 2);
        }

       
    }
}

if ( !function_exists('get_star_ratings'))
{
    function get_star_ratings( $rating )
    {
        $stars = '<ul>
                    <li>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-star"></i></a>
                    </li>
                </ul>';
        if ( is_numeric($rating) && $rating > 0)
        {
            $stars = '<ul>';
            for( $i = 0 ; $i < 5; $i++ )
            {
                if ( ($rating > $i  && is_integer($rating)) || ($rating - $i != 0.5 && !is_integer($rating) && $i < $rating ) )
                {
                    $stars .= '<li><a href="#"><i class="fa fa-star"></i></a></li>';
                }elseif ($rating > $i  && !is_integer($rating) && $rating - $i == 0.5 ){
                    $stars .= '<li><a href="#"><i class="fa fa-star-half"></i></a></li>';
                }else{
                    $stars .= '<li><a href="#"><i class="fa fa-star-o"></i></a></li>';
                }
            }
            $stars .= '</ul>';
        }
        return $stars;
    }
}

if ( !function_exists('is_integer') )
{
    function is_integer($val)
    {
        $num = (int) $val;
        return $num == $val;
    }
}

if ( !function_exists('encrypt'))
{
    function encrypt( $string ) {
        // you may change these values to your own
        $secret_key = 'my_simple_secret_key';
        $secret_iv = 'my_simple_secret_iv';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        return $output;
    }
}

if ( !function_exists('decrypt'))
{
    function decrypt( $string ) {
        // you may change these values to your own
        $secret_key = 'my_simple_secret_key';
        $secret_iv = 'my_simple_secret_iv';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
        
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );    
        return $output;
    }
}


if ( !function_exists('get_current_link'))
{
    function get_current_link() {
       return encrypt(current_url());
    }
}

if ( !function_exists('generate_order_no'))
{
    function generate_order_no($user_id)
    {
        $encdate = base64_encode(date('ymdH'));
        
        $str = implode( '', [$encdate,$user_id] );
        return base64_encode( $str );
    }
}

if ( !function_exists('validate_order_no'))
{
    function validate_order_no($user_id, $order_no)
    {
        $encdate_uid = base64_decode( $order_no );
        $encDate = substr( $encdate_uid, 0, (-1 * strlen($user_id)) );
        $result = substr( $encdate_uid, (-1 * strlen($user_id)) );
        $date = base64_decode( $encDate );
        return $result == $user_id && date('ymdH') == $date;
    }
}


if ( !function_exists('hash_password') )
{
    function hash_password( $password )
    {
        return md5($password . SALT);
    }
}