<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_transactions extends CI_Model {
	protected $table, $table_id;

	public function __construct() {
		parent::__construct();
		$this->table = "transactions";
		$this->table_id = "transaction_id";
	}

	function get_table() {
		return $this->table;
	}

	function set_primary_key($col) {
		$this->table_id = $col;
	}

	function get() {
		$table = $this->get_table();
		$this->db->order_by($this->table_id, 'DESC');
		$query = $this->db->get($table);
		return $query;
	}

	function get_sum($field, $as) {
		$table = $this->get_table();
		$this->db->select_sum($field, $as);
		$query = $this->db->get($table);
		return $query;
	}

	function getToday()
	{
		$table = $this->get_table();
		$this->db->where("DATE( trans_date ) = CURDATE()");
		$query = $this->db->get($table);
		return $query;
	}

	function getThisWeek()
	{
		$table = $this->get_table();
		$this->db->where("trans_date > DATE_SUB(NOW(), INTERVAL 1 WEEK)");
		$query = $this->db->get($table);
		return $query;
	}

	function getThisMonth()
	{
		$table = $this->get_table();
		$this->db->where("trans_date > DATE_SUB(NOW(), INTERVAL 1 MONTH)");
		$query = $this->db->get($table);
		return $query;
	}

	function getThisYear()
	{
		$table = $this->get_table();
		$this->db->where("trans_date > DATE_SUB(NOW(), INTERVAL 1 YEAR)");
		$query = $this->db->get($table);
		return $query;
	}

	function get_where($id) {
		$table = $this->get_table();
		$this->db->order_by($this->table_id, 'DESC');
		$this->db->where($table.'.'.$this->table_id, $id);
		$query = $this->db->get($table);
		return $query;
	}

	function get_where_custom($col, $value = NULL) {
		if (isset($col) && empty($value)) {
			//if $col is an array and $value is not set
			$this->db->where($col);
		} else {
			$this->db->where($col, $value);
		}
		//$this->db->order_by($this->table_id, 'DESC');
		$table = $this->get_table();
		$query = $this->db->get($table);
		return $query;
	}

	public function isTransactionExist($date, $handler_id, $user_id, $card_no, $trans_type, $amount) {
		$this->db->like('trans_date', $date);
		$result = $this->get_where_custom([
			'transaction_by_id' => $handler_id,
			'customer_id'       => $user_id,
			'card_id'           => $card_no,
			'trans_type'        => $trans_type,
			'amount'        => $amount,
		]);

		return ($result->num_rows() > 0);
	}

	function _insert_debit($data) {
		$table = $this->get_table();

		extract($data);
		$this->db->where('customer_id', $user_id);
		$account = $this->db->get('customers');
		$new_balance = 0;
		$w_balance = 0;

		if ($account->num_rows() > 0) {
			//$this->debugger->debug($account->row_array());
			extract($account->row_array());

			$start = $this->db->get_where('cards', ['card_id' => $card_id])->row()->card_start;
		
			$new_balance = $balance - $amount;
			$w_balance = $this->get_withdrawable_balance($user_id, $card_id, $amount, 'debit');
			$trans_status = 'pending';
			$card_count = 0;
			
			$this->db->where('customer_id', $user_id);
			$this->db->update('customers', ['balance' => $new_balance, 'wbalance' => $w_balance]);

			$this->db->where('card_id', $card_id);
			$this->db->update('cards', ['updated_at' => date("Y-m-d H:i:s"), 'status' => 'pending withdrawal']);
			
			//$this->debugger->debug($account->row_array());
			return $this->db->set(array(
				'customer_id' => $user_id,
				'trans_type' => $trans_type,
				'amount' => $amount,
				'transaction_by_id' => $handler_id,
				'trans_status' => $trans_status,
				'card_id' => $card_id,
				'card_count' => $card_count,
			))->insert($table);
			
		}
		return false;
	}

	function _insert_credit($data, $no_days) {
		$table = $this->get_table();

		extract($data[0]);
		$this->db->where('customer_id', $user_id);
		$account = $this->db->get('customers');
		$new_balance = 0;
		$w_balance = 0;
		$total_amount = array_sum(array_column($data, 'amount'));

		if ($account->num_rows() > 0) {
			
			extract($account->row_array());

			$start = $this->db->get_where('cards', ['card_id' => $card_id])->row()->card_start;
			$new_balance = $balance + $total_amount;
			$w_balance = $this->get_withdrawable_balance($user_id, $card_id, $total_amount, 'credit', $data);
			$trans_status = 'completed';
			$card_count = $this->get_last_cardCount($card_id);

			$this->db->where('customer_id', $user_id);
			$this->db->update('customers', ['balance' => $new_balance, 'wbalance' => $w_balance]);
			
			//$this->debugger->debug($account->row_array());
			for($i=0; $i < $no_days; $i++)
			{
				$batch_transaction[] = array(
					'customer_id' => $user_id,
					'trans_type' => $trans_type,
					'amount' => $amount,
					'transaction_by_id' => $handler_id,
					'trans_status' => $trans_status,
					'card_id' => $card_id,
					'card_count' => $card_count + $i,
				);
			}
			$res = $this->db->insert_batch($table,$batch_transaction);

			$card_count = $this->get_last_cardCount($card_id);
			$this->db->where('card_id', $card_id);
			$status = $card_count == 32 ? 'full' : 'active';
			$this->db->update('cards', ['updated_at' => date("Y-m-d H:i:s"), 'status' => $status]);

			return $res;
			
		}
		return false;
	}

	function get_last_cardCount($card_id)
	{
		$count = 0;
		$result = $this->get_where_custom(['card_id' => $card_id, 'trans_type' => 'credit']);
		
		if ($result->num_rows() == 0 ){
			$count = $this->db->get_where('cards', ['card_id' => $card_id])->row()->card_start;
		}else{
			$count = $result->last_row()->card_count + 1;
		}

		return $count;
	}

	function get_withdrawable_balance($user_id, $card_id, $amount, $trans_type, $data = [])
	{
		$balance = 0;
		$obalance = $this->db->where(['customer_id' => $user_id])->get('customers')->row()->wbalance;

		
		if ( $trans_type == 'credit' )
		{
			$result = $this->get_where_custom(['card_id' => $card_id, 'trans_type' => 'credit']);
			//$this->debugger->debug(($obalance + $amount) - $data[0]['amount']);
			if ($result->num_rows() == 0 && empty($data)){
				return $obalance;
			}if ($result->num_rows() == 0 && !empty($data)){
				return ($obalance + $amount) - $data[0]['amount'];
			}
			
			return $obalance + $amount;
		}

		if ( $trans_type == 'debit' ){
			return $obalance - $amount;
		}
		

		return $balance;
	}

	function json_reset_card_transactions($card_id)
	{
		$this->db->select_sum('amount', 'total_profit');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where(['trans_type' => 'credit', 'is_flagged'=> '0']);
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ?$result:0 ;
	}

	function get_total_profit()
	{
		$this->db->select_sum('amount', 'total_profit');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where(['trans_type' => 'credit', 'is_flagged'=> '0']);
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ? $result:0 ;
	}

	function get_daily_profit()
	{
		$this->db->select_sum('amount', 'total_profit', 'trans_date');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where(['trans_type' => 'credit', 'is_flagged'=> '0']);
		$this->db->where("DATE( trans_date ) = CURDATE()");
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ? $result:0 ;
	}

	function get_daily_total()
	{
		$this->db->select_sum('amount', 'total_profit', 'trans_date');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where(['trans_type' => 'credit', 'is_flagged'=> '0']);
		$this->db->where("DATE( trans_date ) = CURDATE()");
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ? $result:0 ;
	}

	function get_monthly_profit()
	{
		$this->db->select_sum('amount', 'total_profit', 'trans_date');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where(["trans_date >" =>"DATE_SUB(NOW(), INTERVAL 1 MONTHLY)"]);
		$this->db->where(['trans_type' => 'credit', 'is_flagged'=> '0']);
		
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ? $result:0 ;
	}

	function get_total_withdrawal()
	{
		$this->db->select_sum('amount', 'total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$result = $this->get_where_custom(['trans_type' => 'debit', 'trans_status' => 'pending', 'is_flagged'=> '0'])->row()->total;
		return is_numeric($result) ?$result:0 ;
	}

	function get_handler_total_profit()
	{
		$user = get_session_data('ssid');

		$this->db->select_sum('amount', 'total_profit');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where(['trans_type' => 'credit', 'transaction_by_id' => $user, 'is_flagged'=> '0']);
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ?$result:0 ;
	}

	function get_handler_daily_profit()
	{
		$user = get_session_data('ssid');

		$this->db->select_sum('amount', 'total_profit');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where("DATE( trans_date ) = CURDATE()");
		$this->db->where(['trans_type' => 'credit', 'transaction_by_id' => $user, 'is_flagged'=> '0']);
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ?$result:0 ;
	}

	function get_handler_daily_total()
	{
		$user = get_session_data('ssid');

		$this->db->select_sum('amount', 'total_profit');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where("DATE( trans_date ) = CURDATE()");
		$this->db->where(['trans_type' => 'credit', 'transaction_by_id' => $user, 'is_flagged'=> '0']);
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ?$result:0 ;
	}

	function get_handler_monthly_profit()
	{
		$user = get_session_data('ssid');

		$this->db->select_sum('amount', 'total_profit');
		$this->db->join('transactions', 'transactions.card_id = cards.card_id');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$this->db->where('transactions.card_count = cards.card_start');
		$this->db->where(["trans_date >" =>"DATE_SUB(NOW(), INTERVAL 1 MONTHLY)"]);
		$this->db->where(['trans_type' => 'credit', 'transaction_by_id' => $user, 'is_flagged'=> '0']);
		$result = $this->db->get('cards')->row()->total_profit;
		return is_numeric($result) ?$result:0 ;
	}

	

	function get_handler_total_withdrawal()
	{
		$user = get_session_data('ssid');
		$this->db->select_sum('amount', 'total');
		$this->db->join('customers', 'transactions.customer_id = customers.customer_id');
		$result = $this->get_where_custom(['trans_type' => 'debit', 'trans_status' => 'pending', 'transaction_by_id' => $user, 'is_flagged'=> '0'])->row()->total;
		return is_numeric($result) ?$result:0 ;
	}

	function _update($id, $data) {
		$table = $this->get_table();
		$this->db->where($this->table_id, $id);
		$this->db->set($data);
		return $this->db->update($table);
	}

	function _delete($id) {
		$table = $this->get_table();
		$this->db->where($this->table_id, $id);
		return $this->db->delete($table);
	}

	function _delete_all() {
		$table = $this->get_table();
		$this->db->where('transaction_id !=','');
		return $this->db->delete($table);
	}

	function _delete_where($col, $value = NULL) {
        $table = $this->get_table();
        if (isset($col) && empty($value)) {
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        return $this->db->delete($table);
    }

	function count_where($column, $value) {
		$table = $this->get_table();
		$this->db->where($column, $value);
		$query = $this->db->get($table);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function count_all() {
		$table = $this->get_table();
		$query = $this->db->get($table);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function get_max() {
		$table = $this->get_table();
		$this->db->select_max($this->table_id);
		$query = $this->db->get($table);
		$row = $query->row();
		$id = $row->id;
		return $id;
	}

	function _custom_query($mysql_query) {
		$query = $this->db->query($mysql_query);
		return $query;
	}

}

/* End of file Mdl_transactiongroups.php */
/* Location: ./application/models/Mdl_transactiongroups.php */
