<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Mdl_Menus extends CI_Model {

	protected $table_id;

	function __construct() {
		parent::__construct();
		$this->set_primary_key('menu_id');
	}

	function get_table() {
		$table = "menus";
		return $table;
	}

	function set_primary_key($col) {
		$this->table_id = $col;
	}

	function get( $all = true, $data = array() ) {
		$menus = array();
		$table = $this->get_table();
		if (!$all) {
			$this->db->where('parent_id', '0');
			$this->db->order_by('menu_order');
			if (array_key_exists('location', $data)) {
				$this->db->where('menu_location', $data['location']);
			}

			$result = $this->db->get($table)->result_array();
			foreach ($result as $key => $menu) {
				$menus[$key] = $menu;
				$this->db->order_by('menu_order');
				$sub_menus = $this->db->get_where($table, ['parent_id' => $menu['menu_id']])->result();
				$menus[$key]['sub_menus'] = $sub_menus;
			}
		} else {
			$menus = $this->db->get($table)->result();
		}

		return $menus;
	}

	function get_where($id) {
		$table = $this->get_table();
		$this->db->where($this->table_id, $id);
		$query = $this->db->get($table);
		return $query;
	}

	function get_where_custom($col, $value = NULL) {
		if (isset($col) && empty($value)) {
//if $col is an array and $value is not set
			$this->db->where($col);
		} else {
			$this->db->where($col, $value);
		}
		$table = $this->get_table();
		$query = $this->db->get($table);
		return $query;
	}

//
	//    function get_with_limit( $limit, $offset, $order_by )
	//    {
	//	$table = $this->get_table();
	//	$this->db->limit( $limit, $offset );
	//	$this->db->order_by( $order_by );
	//	$query = $this->db->get( $table );
	//	return $query;
	//    }

	function _insert($data) {
		$table = $this->get_table();
		return $this->db->insert($table, $data);
	}

	function _update($id, $data) {
		$table = $this->get_table();
		$this->db->where($this->table_id, $id);
		return $this->db->update($table, $data);
	}

	function _update_batch($data, $key) {
		$table = $this->get_table();
		return $this->db->update_batch($table, $data, $key);
	}

	function _delete($id) {
		$table = $this->get_table();
		$this->db->where($this->table_id, $id);
		return $this->db->delete($table);
	}

	function count_where($column, $value) {
		$table = $this->get_table();
		$this->db->where($column, $value);
		$query = $this->db->get($table);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function count_all() {
		$table = $this->get_table();
		$query = $this->db->get($table);
		$num_rows = $query->num_rows();
		return $num_rows;
	}

	function get_max() {
		$table = $this->get_table();
		$this->db->select_max($this->table_id);
		$query = $this->db->get($table);
		$row = $query->row();
		$id = $row->id;
		return $id;
	}

	function _custom_query($mysql_query) {
		$query = $this->db->query($mysql_query);
		return $query;
	}

}
