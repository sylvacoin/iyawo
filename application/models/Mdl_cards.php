<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_cards extends CI_Model {

	protected $table_id = '';

    function __construct() {
        parent::__construct();
        $this->set_primary_key('card_id');
    }

    function get_table() {
        $table = "cards";
        return $table;
    }

    function set_primary_key($col) {
        $this->table_id = $col;
    }

    function get($order_by = NULL) {
        $table = $this->get_table();
        if ( $order_by )
        {
        	$this->db->order_by($order_by, 'DESC');
        }
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($id) {
    	$this->db->order_by($this->table_id, 'DESC');
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_custom($col, $value = NULL) {
        if (isset($col) && empty($value)) {//if $col is an array and $value is not set
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        $table = $this->get_table();
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_custom_join($col, $value = NULL) {
        if (isset($col) && empty($value)) {//if $col is an array and $value is not set
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        $this->db->select('cards.*, sum(t.amount) as total');
        $this->db->join('transactions t', 't.card_id = cards.card_id', 'left');
        $this->db->group_by('cards.card_id');
        $table = $this->get_table();
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data) {
        $table = $this->get_table();
        return $this->db->insert($table, $data);
    }

    function _update($id, $data) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        $this->db->set($data);
        return $this->db->update($table);
    }

    function _delete($id) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        return $this->db->delete($table);
    }

    function _delete_where($col, $value = NULL) {
        $table = $this->get_table();
        if (isset($col) && empty($value)) {//if $col is an array and $value is not set
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        return $this->db->delete($table);
    }

    function count_where($column, $value) {
        $table = $this->get_table();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function count_all() {
        $table = $this->get_table();
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_max() {
        $table = $this->get_table();
        $this->db->select_max($this->table_id);
        $query = $this->db->get($table);
        $row = $query->row();
        $id = $row->id;
        return $id;
    }

    function _custom_query($mysql_query) {
        $query = $this->db->query($mysql_query);
        return $query;
    }

    public function getCardBalance($card_id) {
        $result = $this->get_where($card_id);
        return $result->row()->card_wbalance;
    }

    function get_pending_withdrawals()
    {
        $this->db->join('customers', 'cards.customer_id = customers.customer_id');
        $this->db->select_sum('card_wbalance', 'total');
        $result = $this->get_where_custom(['status' => 'pending withdrawal'])->row()->total;
        return is_numeric($result) ?$result:0 ;
    }

    function get_pending_withdrawal_count()
    {
        $this->db->join('customers', 'cards.customer_id = customers.customer_id');
        $result = $this->get_where_custom(['status' => 'pending withdrawal']);
        return $result->num_rows() ;
    }

    function _delete_all() {
        $table = $this->get_table();
        $this->db->where($this->table_id.'!=', '');
        return $this->db->delete($table);
    }
}

/* End of file Mdl_auth.php */
/* Location: ./application/models/Mdl_auth.php */