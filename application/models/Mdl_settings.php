<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_settings extends CI_Model {

	public $variable;

	public function __construct() {
        parent::__construct();
        $this->set_primary_key('card_id');
    }

    public function get_table() {
        $table = "settings";
        return $table;
    }

    public function set_primary_key($col) {
        $this->table_id = $col;
    }

    public function get($order_by = NULL) {
        $table = $this->get_table();
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    public function get_where($id) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        $query = $this->db->get($table);
        return $query;
    }

    public function get_where_custom($col, $value = NULL) {
        if (isset($col) && empty($value)) {//if $col is an array and $value is not set
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        $table = $this->get_table();
        $query = $this->db->get($table);
        return $query;
    }

    public function count_where($column, $value) {
        $table = $this->get_table();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function count_all() {
        $table = $this->get_table();
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function get_max() {
        $table = $this->get_table();
        $this->db->select_max($this->table_id);
        $query = $this->db->get($table);
        $row = $query->row();
        $id = $row->id;
        return $id;
    }

    public function _custom_query($mysql_query) {
        $query = $this->db->query($mysql_query);
        return $query;
    }
}

/* End of file Mdl_settings.php */
/* Location: ./application/models/Mdl_settings.php */