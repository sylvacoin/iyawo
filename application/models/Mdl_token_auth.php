<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_token_auth extends CI_Model {

	 protected $table_id;

    function __construct() {
        parent::__construct();
        $this->set_primary_key('id');
    }

    function get_table() {
        $table = "token_auth";
        return $table;
    }

    function set_primary_key($col) {
        $this->table_id = $col;
    }

    function get($order_by = NULL) {
        $table = $this->get_table();
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($id) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_custom($col, $value = NULL) {
        if (isset($col) && empty($value)) {//if $col is an array and $value is not set
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        $table = $this->get_table();
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data) {
        $table = $this->get_table();
        return $this->db->insert($table, $data);
    }

    function _update($id, $data) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        return $this->db->update($table, $data);
    }

    function _delete($id) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        return $this->db->delete($table);
    }

    function getTokenByUsername( $email, $expired )
	{
		$this->db->where(['username'=>$email, 'is_expired'=>$expired]);
		return $this->db->get('token_auth')->row();
	}

	function markAsExpired($tokenId) {

		$table = $this->get_table();
		return $this->db->update($table, ['is_expired'=> 1],['id' => $tokenId]);
    }

}

/* End of file Mdl_token_auth.php */
/* Location: ./application/models/Mdl_token_auth.php */