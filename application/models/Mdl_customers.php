<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_customers extends CI_Model {

	protected $table_id = '';

    function __construct() {
        parent::__construct();
        $this->set_primary_key('customer_id');
    }

    function get_table() {
        $table = "customers";
        return $table;
    }

    function set_primary_key($col) {
        $this->table_id = $col;
    }

    function get($order_by = NULL) {
        $table = $this->get_table();
        if ( $order_by )
        {
        	$this->db->order_by($order_by, 'DESC');
        }
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($id) {
    	$this->db->order_by($this->table_id, 'DESC');
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_custom($col, $value = NULL) {
        if (isset($col) && empty($value)) {//if $col is an array and $value is not set
            $this->db->where($col);
        } else {
            $this->db->where($col, $value);
        }
        $table = $this->get_table();
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data) {
        $table = $this->get_table();
        return $this->db->insert($table, $data);
    }

    function _update($id, $data) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        $this->db->set($data);
        return $this->db->update($table);
    }

    function _update_all($data) {
        $table = $this->get_table();
        $this->db->where('customer_id !=','');
        $this->db->set($data);
        return $this->db->update($table);
    }

    function _delete($id) {
        $table = $this->get_table();
        $this->db->where($this->table_id, $id);
        return $this->db->delete($table);
    }

    function _delete_all() {
        $table = $this->get_table();
        $this->db->where($this->table_id.'!=', '');
        return $this->db->delete($table);
    }

    function count_where($column, $value) {
        $table = $this->get_table();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function count_all() {
        $table = $this->get_table();
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_max() {
        $table = $this->get_table();
        $this->db->select_max($this->table_id);
        $query = $this->db->get($table);
        $row = $query->row();
        $id = $row->id;
        return $id;
    }

    function _custom_query($mysql_query) {
        $query = $this->db->query($mysql_query);
        return $query;
    }

    public function getUserBalance($usr_id) {
        $result = $this->get_where($usr_id);
        return $result->row()->wbalance;
    }

    public function getUserOBalance($usr_id) {
        $result = $this->get_where($usr_id);
        return $result->row()->balance;
    }

     function get_total_balance()
    {
        $this->db->select_sum('wbalance', 'total');
        $result = $this->get_where_custom([])->row()->total;
        return is_numeric($result) ?$result:0 ;
    }



    function get_handler_total_balance()
    {
        $user = get_session_data('ssid');
        $this->db->select_sum('wbalance', 'total');
        $result = $this->get_where_custom(['handler_id' => $user])->row()->total;
        return is_numeric($result) ?$result:0 ;
    }

}

/* End of file Mdl_auth.php */
/* Location: ./application/models/Mdl_auth.php */