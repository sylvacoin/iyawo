Vue.component('nested-draggable', {
	template: `
		<draggable tag="ul" :list="tasks" :class="{'py-1':true, 'dragArea':draggingInfo}" handle=".draggable" v-bind="dragOptions"
        @start="drag = true"
        @end="drag = false" >
      
        <li v-for="(el, idx) in tasks" :key="el.label" class="card">
            <div class="rounded border mb-2">
                  <div class="card-body p-2">
                    <div class="media">
                      <i class="icon-menu draggable icon-sm align-self-center mr-3"></i>
                      <div class="media-body">
                        <h6 class="mb-1">{{ el.label }}</h6>
                        <p class="mb-0 text-muted">
                         <small>{{el.url}}</small>
                        </p>
                      </div>
                      <i class="icon-close close  align-self-center ml-3" @click="removeAt(idx)"></i>
                    </div>
                  </div>
            </div>
            <nested-draggable :tasks="el.sub_menus" v-if="el.sub_menus"/>
        </li>
      
    </draggable>
  `,
  props: {
	tasks: {
      required: true,
      type: Array
    }
  },
  data(){
  	return {
  		drag: false
  	}
  },
  computed: {
      dragOptions() {
        return {
          animation: 200,
          group: "description",
          disabled: false,
          ghostClass: "ghost"
        };
      },
      draggingInfo() {
	      return this.drag ? true : false;
      }
    },
 methods: {
      sort() {
        this.list = this.list.sort((a, b) => a.order - b.order);
      },
      removeAt(idx) {
        this.list.splice(idx, 1);
      },
  },
  
})
